#ifndef RRSE_QUERY_H_
#define RRSE_QUERY_H_

#include "RRTQuery.h"

#include "Utilities/MetricUtils.h"
#include <containers/sequential/graph/algorithms/astar.h>

////////////////////////////////////////////////////////////////////////////////
/// @ingroup MapEvaluators
/// @brief Evaluate a roadmap under construction to see if a query has been
///        satisfied.
/// @tparam MPTraits Motion planning universe
///
/// This query is specialized for RRT methods. The first query point is treated
/// as the start, and the remaining points are treated as sub-goals. This object
/// will attempt to find a path from the start through all sub-goals. If a
/// particular sub-goal is not already in the map, an extension towards it will
/// be attempted if the nearest node is within the provided extender's range.
////////////////////////////////////////////////////////////////////////////////
template <typename MPTraits>
class RRSEQuery : public RRTQuery<MPTraits> {
  
  public:

    ///\name Motion Planning Types
    ///@{

    typedef typename MPTraits::CfgType          CfgType;
    typedef typename MPTraits::CfgRef           CfgRef;
    typedef typename MPTraits::WeightType       WeightType;
    typedef typename MPTraits::MPProblemType    MPProblemType;
    typedef typename MPProblemType::RoadmapType RoadmapType;
    typedef typename MPProblemType::GraphType   GraphType;
    typedef typename MPProblemType::VID         VID;

    ///@RRSE.h
    ///\name Construction
    ///@{

    RRSEQuery();
    RRSEQuery(MPProblemType* _problem, XMLNode& _node);
    virtual ~RRSEQuery() = default;

    ///@}
    ///\name MPBaseObject Overrides
    ///@{

    void ParseXML(XMLNode& _node);
    virtual void Print(ostream& _os) const override;

    ///@}
    ///\name Query Interface
    ///@{
    /*
    const CfgType& GetRandomGoal() const {
      if(m_goals.empty())
        throw RunTimeException(WHERE, "Random goal requested, but none are "
            "available.");
      return m_goals[LRand() % m_goals.size()];
    }
     */ 

     //virtual bool PerformMultiGoal(vector<CfgType>& _allgoals);
    ////////////////////////////////////////////////////////////////////////////
    /// \brief Check whether a path connecting a given start and goal exists in
    ///        the roadmap.
    /// \param[in] _start The starting configuration to use.
    /// \param[in] _goal  The goal configuration to use.
    /// \return A bool indicating whether the path was found.
    //\name RRTQuery overrides
    virtual bool PerformSubQuery(const CfgType& _start, const CfgType& _goal)
        override;
    
     virtual void Initialize(RoadmapType* _r = nullptr) override;
  
    protected:
    /*
    ///\name Helpers
    ///@{

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Find the nearest node to a goal configuration, assuming that the
    ///        goal is not already in the map.
    /// \param[in] _goal The goal node of interest.
    /// \return The descriptor of the map node that is nearest to _goal, or
    ///         INVALID_VID if we have already checked every node in the map. The
    ///         second item is the distance from said node to _goal.
    pair<VID, double> FindNearestNeighbor(const CfgType& _goal);

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Find the nearest node to a goal configuration that is already
    ///        connected to the start node. The start is assumed to be in the map,
    ///        but the goal may or may not be.
    /// \param[in] _start The descriptor of the start node in the map.
    /// \param[in] _goal  The goal configuration, possibly in the map.
    /// \return If the goal isn't in the map, return the descriptor of the map
    ///         node that is nearest to it and their separation distance. If
    ///         the goal is already in the map, return its VID if it is connected
    ///         to the start and INVALID_VID otherwise.
    pair<VID, double> FindNearestConnectedNeighbor(const VID _start,
        const CfgType& _goal);

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Extend towards a goal from the nearest node in the map.
    /// \param[in] _nearest The descriptor of the nearest node and its distance
    ///                     to _goal.
    /// \param[in] _goal The goal configuration to extend towards.
    /// \return The descriptor of the newly extended node and its distance to
    ///         _goal, or INVALID_VID if the extension failed.
    pair<VID,double> ExtendToGoal(const pair<VID, double>& _nearest,
        const CfgType& _goal) const;
    */
    ////////////////////////////////////////////////////////////////////////////
    /// \brief Extend towards a blocked node from the nearest node in the map.
    /// \param[in] _nearest The descriptor of the nearest node and its distance
    ///                     to _cfg.
    /// \param[in] _cfg The blocked node configuration to extend towards.
    /// \return The descriptor of the newly extended node and its distance to
    ///         _cfg, or INVALID_VID if the extension failed.
    pair<VID,double>TreeConnect(const CfgType& _cfg);

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Process the witness node queue. Iterative version stops when
    ///        a valid node is popped and start and goal are connected.
    /// \param[in] _start The query start.
    /// \param[in] _goal The query goal.
    void ProcessQueue(const VID _start, const VID _goal);

    ///@}
    ///\name Query State
    ///@{

    //double m_goalDist{0.};      ///< Getting at least this close = success.
    //VID m_highestCheckedVID;   ///< The highest VID we have tried to connect to the goal.
    deque<CfgType> m_q;   ///< A queue of both free and blocked witness nodes.
    double m_radius{0.};   ///< radius of circular region with witness node as center.

    ///@}
    ///\name MP Object Labels
    ///@{

    string m_nfLabel{"Nearest"};       ///< Neighborhood finder label.
    string m_exLabel{"BERO"};          ///< Extender label.

    ///@}
    ///\name Unhide RRTQuery names.
    ///@{

   // using QueryMethod<MPTraits>::m_query;
    using RRTQuery<MPTraits>::m_path;

    ///@}
};

/*----------------------------- Construction ---------------------------------*/

template <typename MPTraits>
RRSEQuery<MPTraits>::
RRSEQuery() : RRTQuery<MPTraits>() {
  this->SetName("RRSEQuery");
}


template <typename MPTraits>
RRSEQuery<MPTraits>::
RRSEQuery(MPProblemType* _problem, XMLNode& _node) :
    RRTQuery<MPTraits>(_problem, _node) {
  this->SetName("RRSEQuery");
  ParseXML(_node);
}

/*--------------------------- MPBaseObject Overrides -------------------------*/

template <typename MPTraits>
void
RRSEQuery<MPTraits>::
ParseXML(XMLNode& _node) {
 /* m_nfLabel = _node.Read("nfLabel", false, m_nfLabel, "Neighborhood finder "
      "method");
  
  m_exLabel = _node.Read("exLabel", true, m_exLabel, "Extender method");
  
  m_goalDist = _node.Read("goalDist", false, 0., 0.,
      numeric_limits<double>::max(), "Minimun Distance for valid query");
*/
  m_radius = _node.Read("radius", true, 0., 0.,
      numeric_limits<double>::max(), "radial distance for witness node");
}


template <typename MPTraits>
void
RRSEQuery<MPTraits>::
Print(ostream& _os) const {
  RRTQuery<MPTraits>::Print(_os);
  _os //<< "\n\tNeighborhood Finder: " << m_nfLabel
      //<< "\n\tExtender: " << m_exLabel
      << "\n\tRadial Distance: " << m_radius << endl;
      //<< "\n\tGoal distance: " << m_goalDist << endl;
}


/*--------------------------- RRTQuery overrides --------------------------------*/

/*template <typename MPTraits>
bool
RRSEQuery<MPTraits>::
PerformMultiGoal(vector<CfgType>& _allgoals){
copy(m_query.begin(), m_query.end(), back_inserter(_allgoals));
typedef typename vector<CfgType>::iterator GIT;
for(GIT git = _allgoals.begin(); git != _allgoals.end(); ++git) {
  cout<<"Goal coordinates :" << *git <<endl;

  CfgType start, goal;
    if(git == _allgoals.end())
       break;
    else
       start = *git;
       goal = *git++;
      return PerformSubQuery(start, goal);
 }
 return false;
}*/

template <typename MPTraits>
bool
RRSEQuery<MPTraits>::
PerformSubQuery(const CfgType& _start, const CfgType& _goal) {

  VID sVID = INVALID_VID, gVID = INVALID_VID; 
 do {
    // Extract paths until a valid one is found or start and goal become
    // disconnected.
    if(RRTQuery<MPTraits>::PerformSubQuery(_start, _goal))
      return true;

    if(gVID == INVALID_VID) {
      auto g = m_path->GetRoadmap()->GetGraph();
      sVID = g->GetVID(_start);
      gVID = g->GetVID(_goal);
    }

    ProcessQueue(sVID, gVID);

    // Keep looping if the new connected nodes put start and goal in same CC.
  } while(this->SameCC(sVID, gVID));

  // Start and goal not connected anymore, go back to sampling
  if(this->m_debug)
    cout << "\tRRSEQuery::PerformQuery returning false." << endl;

  return false;

/*
  if(this->m_debug)
    cout << "Evaluating sub-query:" << endl
         << "\tfrom " << _start << endl
         << "\tto   " << _goal << endl;

  VID start = m_path->GetRoadmap()->GetGraph()->GetVID(_start);
  pair<VID, double> nearest;
  bool success = false;

  // Find the nearest node to _goal that is also connected to _start.
  nearest = FindNearestConnectedNeighbor(start, _goal);
  if(nearest.first == INVALID_VID)
    // If the nearest node is invalid, it means that the goal is in the map and
    // not connected to start. In this case, we can't connect.
    success = false;
  else if(nearest.second <= m_goalDist)
    // The nearest node is within the goal distance, so we are close enough.
    success = true;
  else {
    // The nearest node is too far and the goal isn't already connected. Try to
    // extend toward goal if we are within extender's delta range. If we can't
    // extend, we can't connect.
    nearest = ExtendToGoal(nearest, _goal);
    success = nearest.first != INVALID_VID && nearest.second <= m_goalDist;
  }

  if(success) {
    this->GeneratePath(start, nearest.first);
    if(this->m_debug)
      cout << "\tSuccess: found path from start to nearest node "
           << nearest.first << " at a distance of " << nearest.second
           << " from the goal." << endl;
  }
  else if(this->m_debug)
    cout << "\tFailed to connect (goal distance is " << m_goalDist << ").\n";

  return success;*/
}


template <typename MPTraits>
void
RRSEQuery<MPTraits>::
Initialize(RoadmapType* _r) {
  RRTQuery<MPTraits>::Initialize(_r);
 // m_highestCheckedVID = 0;
}

/*------------------------------- Helpers ------------------------------------*/

/*
template <typename MPTraits>
pair<typename RRSEQuery<MPTraits>::VID, double>
RRSEQuery<MPTraits>::
FindNearestNeighbor(const CfgType& _goal) {
  if(this->m_debug)
    cout << "\t\tFinding the nearest node to the goal..." << endl;

  auto g = m_path->GetRoadmap()->GetGraph();

  VID highestVID = g->get_num_vertices() - 1;

  // Quit if we have already tried with the current set of vertices.
  if(m_highestCheckedVID == highestVID) {
    if(this->m_debug)
      cout << "\t\t\tAll nodes have already been checked." << endl;
    return make_pair(INVALID_VID, numeric_limits<double>::max());
  }
  else if(this->m_debug)
    cout << "\t\t\tNodes 0 through " << m_highestCheckedVID
         << " have already been checked." << endl
         << "\t\t\tSearching nodes " << m_highestCheckedVID + 1 << " through "
         << highestVID << "..." << endl;

  auto stats = this->GetStatClass();

  // If we haven't tried all vertexes, find the nearest untried vertex.
  stats->StartClock("RRSEQuery::NeighborhoodFinding");
  vector<pair<VID, double>> neighbors;
  this->GetNeighborhoodFinder(m_nfLabel)->FindNeighbors(m_path->GetRoadmap(),
      ++g->find_vertex(m_highestCheckedVID), g->end(), true, _goal,
      back_inserter(neighbors));
  m_highestCheckedVID = highestVID;
  stats->StopClock("RRSEQuery::NeighborhoodFinding");

  if(this->m_debug)
    cout << "\t\t\tFound nearest node " << neighbors.back().first << " at "
         << "distance " << neighbors.back().second << "." << endl;

  return neighbors.back();
}


template <typename MPTraits>
pair<typename RRSEQuery<MPTraits>::VID, double>
RRSEQuery<MPTraits>::
FindNearestConnectedNeighbor(const VID _start, const CfgType& _goal) {
  if(this->m_debug)
    cout << "\tSearching for the nearest connected neighbor..." << endl;

  auto g = m_path->GetRoadmap()->GetGraph();
  pair<VID, double> nearest;

  if(g->IsVertex(_goal)) {
    // The goal is already in the roadmap. It is it's own neighbor if it shares
    // a CC with _start and disconnected otherwise.
    VID goal = g->GetVID(_goal);
    if(this->SameCC(_start, goal))
      nearest = make_pair(goal, 0);
    else
      nearest = make_pair(INVALID_VID, numeric_limits<double>::max());
  }
  else
    // If _goal isn't already connected, find the nearest connected node.
    nearest = FindNearestNeighbor(_goal);

  if(this->m_debug) {
    if(nearest.first != INVALID_VID)
      cout << "\t\tClosest neighbor to goal is node " << nearest.first << " at "
           << "distance " << setprecision(4) << nearest.second << ".\n";
    else
      cout << "\t\tNo valid node was found." << endl;
  }
  return nearest;
}


template <typename MPTraits>
pair<typename RRSEQuery<MPTraits>::VID, double>
RRSEQuery<MPTraits>::
ExtendToGoal(const pair<VID, double>& _nearest, const CfgType& _goal) const {
  auto g = m_path->GetRoadmap()->GetGraph();
  auto e = this->GetExtender(m_exLabel);

  VID newVID = INVALID_VID;
  double distance = numeric_limits<double>::max();

  // If the nearest node is outside the extender's range, return invalid.
  if(_nearest.second > e->GetMaxDistance())
    return make_pair(newVID, distance);

  if(this->m_debug)
    cout << "\tTrying extension from node " << _nearest.first
         << " toward goal.\n";

  // Otherwise, try to extend from _nearest to _goal.
  CfgType qNew;
  LPOutput<MPTraits> lpOutput;
  if(e->Extend(g->GetVertex(_nearest.first), _goal, qNew, lpOutput)) {
    distance = lpOutput.m_edge.first.GetWeight();
    // If we went far enough, add the new node and edge.
    if(distance > e->GetMinDistance() && !g->IsVertex(qNew)) {
      newVID = g->AddVertex(qNew);
      qNew.SetStat("Parent", _nearest.first);
      g->AddEdge(_nearest.first, newVID, lpOutput.m_edge);
      // Using the NeighborhoodFinder's Distance metric for consistency.
      distance = this->GetNeighborhoodFinder(m_nfLabel)->GetDMMethod()->
          Distance(qNew, _goal);
      if(this->m_debug)
        cout << "\t\tExtension succeeded, created new node " << newVID << " at "
             << "distance " << distance << " from goal." << endl;
    }
    else if(this->m_debug)
      cout << "\t\tExtension failed." << endl;
  }

  return make_pair(newVID, distance);
}
*/

template <typename MPTraits>
void
RRSEQuery<MPTraits>::
ProcessQueue(const VID _start, const VID _goal){
if(this->m_debug)
    cout << "\tProcessing the queue..." << endl;

  while(!m_q.empty()) {
    CfgType cfg = m_q.front();
    m_q.pop_front();

    if(cfg.GetLabel("VALID")) {
      RRTQuery<MPTraits>::ExtendToGoal(RRTQuery<MPTraits>::FindNearestNeighbor(cfg), cfg);
      if(this->SameCC(_start, _goal))
        break;
    }
    else
      TreeConnect(cfg);
  }

  if(this->m_debug)
    cout << "\tQueue processing complete." << endl;

}


template <typename MPTraits>
pair<typename RRSEQuery<MPTraits>::VID, double>
RRSEQuery<MPTraits>::
TreeConnect(const CfgType& _cfg){
pair<VID, double> nearest = RRTQuery<MPTraits>::FindNearestNeighbor(_cfg);

auto g = m_path->GetRoadmap()->GetGraph();
auto e = this->GetExtender(m_exLabel);

  VID newVID = INVALID_VID;
  double distance = numeric_limits<double>::max();

  // If the nearest node is outside the radial range, return invalid.
  if(nearest.second > 2 * m_radius)
    return make_pair(newVID, distance);

  if(this->m_debug)
    cout << "\tTrying extension from node " << nearest.first
         << " toward goal.\n";

  // Otherwise, try to extend from nearest to blocked node.
  CfgType qNew;
  LPOutput<MPTraits> lpOutput;
  if(e->Extend(g->GetVertex(nearest.first), _cfg, qNew, lpOutput)) {
    distance = lpOutput.m_edge.first.GetWeight();
    // If we went far enough, add the new node and edge.
    if(distance > m_radius/2 && !g->IsVertex(qNew)) {
      newVID = g->AddVertex(qNew);
      qNew.SetStat("Parent", nearest.first);
      g->AddEdge(nearest.first, newVID, lpOutput.m_edge);
      // Using the NeighborhoodFinder's Distance metric for consistency.
      distance = this->GetNeighborhoodFinder(m_nfLabel)->GetDMMethod()->
          Distance(qNew, _cfg);
      if(this->m_debug)
        cout << "\t\tExtension succeeded, created new node " << newVID << " at "
               << "distance " << distance << " from goal." << endl;
    }
    else if(this->m_debug)
      cout << "\t\tExtension failed." << endl;
  }

  return make_pair(newVID, distance);

}


/*------------------------------------------------------------------------------------------------*/
#endif
