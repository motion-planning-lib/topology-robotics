#ifndef HPS_H_
#define HPS_H_

#include "MPStrategyMethod.h"
#include "Environment/MultiBody.h"
#include "Environment/Body.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct Regions {
    bool c_free, c_narrow, blocked;

    Regions() : c_free(false), c_narrow(false), blocked(false) {}
    ~Regions() {}

};

struct Compare {
 //  template <typename> T
   bool operator()(pair<string,double> left, pair<string,double> right)
    {
        return left.second < right.second;
     }
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

template<class MPTraits>
class HPS : public MPStrategyMethod<MPTraits> {
  public:
    typedef typename MPTraits::CfgType CfgType;
    typedef typename MPTraits::MPProblemType MPProblemType;
    typedef typename MPProblemType::RoadmapType RoadmapType;
   // typedef typename RoadmapType::GraphType GraphType;
    typedef typename MPProblemType::GraphType GraphType;
    typedef typename MPProblemType::SamplerPointer SamplerPointer;
    typedef typename MPProblemType::ConnectorPointer ConnectorPointer;
    typedef typename GraphType::vertex_descriptor VID;
    typedef typename MPProblemType::NeighborhoodFinderPointer NeighborhoodFinderPointer;    

    HPS() {
     this->SetName("HPS");
     }

    HPS(string _samplerSelectionDistribution,
    bool _countCost, double _percentageRandom, bool _fixedCost, bool _resettingLearning,
    int _binSize, string _dm, const map<string, pair<int, int> >& _samplerLabels,
    const vector<string>& _connectorLabels, const vector<string>& _evaluatorLabels);


    HPS(typename MPTraits::MPProblemType* _problem, XMLNode& _node);
    virtual ~HPS() {}

    virtual void ParseXML(XMLNode& _node);
    virtual void Print(ostream& _os) const;

    virtual void Initialize();
    virtual void Run();
    virtual void Finalize();

    void InitializeWeightsProbabilitiesCosts();
    void CopyLearnedProbToProbUse();
    void PrintWeightsProbabilitiesCosts(ostream& _out);

    string SelectNextSamplingMethod(bool _learning);

    double ComputeVisibilityReward(double _samplertime, double _visibility, int _prevCcCount, int _currCcCount, Regions& _space);

    bool InLearningWindow(int _totalSamples) const;

    void RewardAndUpdateWeightsProbabilities(string _method, double _rew,unsigned long int _cost);
    

  protected:
    vector<string> m_samplerLabels;
    vector<string> m_connectorLabels;

    map<string,double> m_nodeWeights;
    map<string,double> m_nodeProbability;
    map<string,double> m_uniformProbability;
    map<string,double> m_learnedProbability;
    map<string,double> m_noCostProbability;
    map<string, unsigned long int> m_nodeCosts;
    map<string,int> m_nodeNumSampled;
    map<string,int> m_nodeNumOversampled;

    string m_localNF;
    double m_percentageRandom; //lambda
    double m_windowPercent;
    bool m_countCost;
    bool m_fixedCost;
    bool m_resettingLearning;
    int  m_binSize;
    string m_samplerSelectionDistribution;
    string m_dmLabel;

};


template<class MPTraits>
HPS<MPTraits>::
HPS(string _samplerSelectionDistribution,
    bool _countCost, double _percentageRandom, bool _fixedCost,
    bool _resettingLearning, int _binSize, string _dm,
    const map<string, pair<int, int>>& _samplerLabels, 
    const vector<string>& _connectorLabels,
    const vector<string>& _evaluatorLabels) :
    m_samplerSelectionDistribution(_samplerSelectionDistribution),
    m_countCost(_countCost), m_percentageRandom(_percentageRandom),
    m_fixedCost(_fixedCost), m_resettingLearning(_resettingLearning),
    m_binSize(_binSize), m_dmLabel(_dm), m_samplerLabels(_samplerLabels), 
    m_connectorLabels(_connectorLabels) {
  this->m_meLabels = _evaluatorLabels;
  this->SetName("HPS");
}

template<class MPTraits>
HPS<MPTraits>::
HPS(typename MPTraits::MPProblemType* _problem, XMLNode& _node) :
    MPStrategyMethod<MPTraits>(_problem, _node) {
  this->SetName("HPS");
  ParseXML(_node);
}

template<class MPTraits>
void HPS<MPTraits>::
ParseXML(XMLNode& _node) {
  for(auto& child : _node) {
    if(child.Name() == "node_generation_method"){
      string generationMethod = child.Read("Method",true,"","Method");
      m_samplerLabels.push_back(generationMethod);
      int initialCost = child.Read("initialCost",false,1,1,MAX_INT,"initialCost");
      m_nodeCosts[generationMethod] = initialCost;
    }
    else if(child.Name() == "node_connection_method")
      m_connectorLabels.push_back(child.Read("Method",true,"","Method"));
    else if(child.Name() == "evaluation_method")
      this->m_meLabels.push_back(
          child.Read("Method", true, "", "Evaluation Method"));
 }

  m_localNF = _node.Read("LocalNeighborFinder",true,"","Local Neighbor Finder" );
  m_percentageRandom = _node.Read("percent_random", true, 0.5, 0.0, 1.0, "percent_random");
  m_binSize = _node.Read("bin_size", true, 5, 1, MAX_INT, "bin_size");
  m_windowPercent = _node.Read("window_percent", true, 0.5, 0.0, 1.0, "window_percent");
  m_countCost = _node.Read("Count_Cost", true, true, "Count_Cost");
  m_fixedCost = _node.Read("fixed_cost", true, false, "fixed_cost");
  m_resettingLearning = _node.Read("resetting_learning", true, false, "resetting_learning");
  m_samplerSelectionDistribution = _node.Read("sampler_selection_distribution", false, "", "sampler_selection_distribution");
  if (m_samplerSelectionDistribution == "nowindow")
    m_windowPercent = 1.0; // 100% of the time learning
  m_dmLabel = _node.Read("dmLabel",true,"","Distance Metric");
}

template<class MPTraits>
void
HPS<MPTraits>::Print(ostream& _os) const {
  _os << "HPS<MPTraits>::\n";
  _os << "\tpercent_random = " << m_percentageRandom << endl;
  _os << "\tbin_size = " << m_binSize << endl;
  _os << "\twindow_percent = " << m_windowPercent << endl;
  _os << "\tcount_cost = " << m_countCost << endl;
  _os << "\tfixed_cost = " << m_fixedCost << endl;
  _os << "\tresetting_learning = " << m_resettingLearning << endl;
  _os << "\tsampler_selection_distribution = " << m_samplerSelectionDistribution << endl;
  _os << "\tDistance Metric = " << m_dmLabel << endl;

  _os << "\tnode_generation_methods: ";
  for(auto&  l : m_samplerLabels)
    _os << l << " ";
  _os << "\n\tnode_connection_methods: ";
  for(auto&  l : m_connectorLabels)
    _os << l << " ";
  _os << "\n\tevaluator_methods: ";
  for(auto&  l : this->m_meLabels)
    _os << l << " ";

}

template<class MPTraits>
void HPS<MPTraits>::Initialize(){
  Print(cout);
  this->GetMPProblem()->GetStatClass()->StartClock("Map Generation");

  InitializeWeightsProbabilitiesCosts();
  CopyLearnedProbToProbUse();

}

template<class MPTraits>
void HPS<MPTraits>::Run(){

  cout<<"\nOBPRM, Gauss, Bridge, UniformRandomFree\n";

  StatClass* stats = this->GetMPProblem()->GetStatClass();
  int totalSamples = 0;
  bool mapPassedEvaluation = this->EvaluateMap();
  map<VID, Visibility> visMap;
   NodeTypeCounts nodeTypes;
   
   Regions space;
   map<string,double> Sampler; 
   
   map<string, map<string,double> > A;   

   stapl::sequential::vector_property_map<typename GraphType::GRAPH,size_t > cmap;
   stats->StartClock("Total Node Generation");
  while(!mapPassedEvaluation){

    if(this->m_debug)
    PrintWeightsProbabilitiesCosts(cout);

   do {
      stats->StartClock("Sampler time");
      string nextNodeGen = SelectNextSamplingMethod(InLearningWindow(totalSamples));
//     string nextNodeGen = SelectNextSamplingMethod(false);
      
      if(this->m_debug){
      cout << "selecting sampler \"" << nextNodeGen << "\"\n";
      cout<<"Working:" << this->m_debug << endl;
      }

      unsigned long int numcdbeforegen = stats->GetIsCollTotal();
      vector<CfgType> vectorCfgs;
      SamplerPointer pNodeGen = this->GetMPProblem()->GetSampler(nextNodeGen);
      pNodeGen->Sample(1, 1, this->m_boundary, back_inserter(vectorCfgs));
      
      stats->StopClock("Sampler time");
      Sampler[nextNodeGen] = stats->GetSeconds("Sampler time"); 

      //counts the number of collisions that have been made so far by infeasible configurations created
      unsigned long int numcdaftergen = stats->GetIsCollTotal(); 
      
   for(typename vector<CfgType>::iterator C = vectorCfgs.begin(); C != vectorCfgs.end(); ++C) {
         space.c_free = false;         
         space.c_narrow = false;
         space.blocked = false;

        if(C->IsLabel("VALID") && C->GetLabel("VALID")) {
          cmap.reset();
         
          int nNumPrevCCs = get_cc_count(*(this->GetMPProblem()->GetRoadmap()->GetGraph()), cmap);

          //add node to roadmap
          VID newVID = this->GetMPProblem()->GetRoadmap()->GetGraph()->AddVertex(*C);
          vector<pair<pair<VID,VID>,bool> > connectionattempts;
    	  for(auto&  label : m_connectorLabels) {
            ConnectorPointer connector = this->GetConnector(label);
            connector->ClearConnectionAttempts();
            connector->Connect(this->GetRoadmap(), newVID);
            connectionattempts.insert(connectionattempts.end(),
                connector->ConnectionAttemptsBegin(),
                connector->ConnectionAttemptsEnd());

    	  }

          for(auto&  attempt : connectionattempts) {
            visMap[attempt.first.first].m_attempts++;
            visMap[attempt.first.second].m_attempts++;
            if(attempt.second){
              visMap[attempt.first.first].m_connections++;
              visMap[attempt.first.second].m_connections++;
            }
          }

  
        //  cout<<"e-good value ="<< visMap[newVID].Ratio()<<endl;
           if(visMap[newVID].Ratio() == 1){
               cout<<"free space"<<", ";
                space.c_free = true;
               }
         
           else if(0.3 < visMap[newVID].Ratio() && visMap[newVID].Ratio() < 1){
                             cout<<"Narrow space"<<", ";
                             space.c_narrow = true;
                  }
             else 
                cout<<"Blocked"<<", ";
                space.blocked = true;


          unsigned long int cost = (double)(numcdaftergen - numcdbeforegen) / (double)vectorCfgs.size();
          if(this->m_debug){
	  cout << "avg node gen cost = " << (double)(numcdaftergen - numcdbeforegen) / (double)vectorCfgs.size() << endl;
          cout << "cost used = " << cost << endl;
	  }
          cmap.reset();
          int nNumCurrCCs = get_cc_count(*(this->GetMPProblem()->GetRoadmap()->GetGraph()), cmap);

          pair<string,double>min = *min_element(Sampler.begin(), Sampler.end(), Compare());
          
          for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
                if(*NG == min.first){
                   if(space.c_free){
                      A["free"][*NG] += 1;
                    }
                   else if(space.c_narrow){
                      A["narrow"][*NG] += 1;
                    }
                   else if(space.blocked){
                      A["blocked"][*NG] += 1;
                    } 
                 }
              }        

          double reward = ComputeVisibilityReward(min.second, visMap[newVID].Ratio(), nNumPrevCCs, nNumCurrCCs, space);
          
	  if(InLearningWindow(totalSamples)){
    	    RewardAndUpdateWeightsProbabilities(min.first/*nextNodeGen*/, reward, cost);
            if(this->m_debug){
	    cout << "new weights and probabilities:";
            PrintWeightsProbabilitiesCosts(cout);
	    }
          }
          else
          m_nodeNumSampled[nextNodeGen]++;

          ++totalSamples;
          
          Sampler.clear();
                   
	}
          if(totalSamples % m_binSize == 0) {
            if(m_samplerSelectionDistribution == "nowindow"){
              CopyLearnedProbToProbUse();
              PrintWeightsProbabilitiesCosts(cout);
            }
    	    if(m_resettingLearning){
              InitializeWeightsProbabilitiesCosts();
              PrintWeightsProbabilitiesCosts(cout);
            }
    	  }

          cmap.reset();
          
      }
     
      	} while((totalSamples % m_binSize) > 0);
      	mapPassedEvaluation = this->EvaluateMap();

      }

     stats->StopClock("Total Node Generation");
     if(this->m_debug) {
        stats->PrintClock("Total Node Generation", cout);
       }

  double reg[3] = {0};
  int i = 0;
  for(map<string, map<string, double> >::const_iterator MPR = A.begin(); MPR != A.end(); ++MPR){
         Sampler = MPR->second;
         cout<<"\nRegion :"<<MPR->first<<"\n"<<endl;
     for(map<string, double>::const_iterator M = Sampler.begin(); M != Sampler.end(); ++M){
            reg[i] = reg[i] + M->second;
        }
     cout<<reg[i]<<endl;
     for(map<string, double>::const_iterator M = Sampler.begin(); M != Sampler.end(); ++M){
            cout<<M->first<<"\t"<<M->second/reg[i]<<endl;
        }
         i = i+1;
         Sampler.clear();
     }
}

template<class MPTraits>
void HPS<MPTraits>::Finalize() {
  StatClass* stats = this->GetMPProblem()->GetStatClass();

  //output map
  this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());


  //output stats
  stats->StopClock("Map Generation");
  string outStatname = this->GetBaseFilename() + ".stat";
  std::ofstream  osStat(outStatname.c_str());
  osStat << "NodeGen+Connection Stats" << endl;
  stats->PrintAllStats(osStat, this->GetMPProblem()->GetRoadmap());
  stats->PrintClock("Map Generation", osStat);
  osStat.close();
}

template<class MPTraits>
void
HPS<MPTraits>::
InitializeWeightsProbabilitiesCosts(){
  m_nodeWeights.clear();
  m_nodeProbability.clear();
  m_uniformProbability.clear();
  m_noCostProbability.clear();
  m_learnedProbability.clear();

  double uniformProbability = double(1.0 / m_samplerLabels.size());
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
    m_nodeWeights[*NG] = 1.0;
    m_nodeProbability[*NG] = uniformProbability;
    m_uniformProbability[*NG] = uniformProbability;
    m_noCostProbability[*NG] = uniformProbability;
    m_learnedProbability[*NG] = uniformProbability;
  }
}


template<class MPTraits>
void
HPS<MPTraits>::
CopyLearnedProbToProbUse() {
  m_learnedProbability.clear();
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
    m_learnedProbability[*NG] = m_nodeProbability[*NG];
}


template<class MPTraits>
void
HPS<MPTraits>::
PrintWeightsProbabilitiesCosts(ostream& _out) {
  _out << endl;
  _out << "Sampler::\tWeight\tProNoCost\tPro\tCost\n";
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
    _out << *NG << "::";
    _out << "\t" << m_nodeWeights[*NG];
    _out << "\t" << m_noCostProbability[*NG];
    _out << "\t" << m_nodeProbability[*NG];
    _out << "\t" << m_nodeCosts[*NG];
    _out << "\n";
  }
  _out << endl;
}


template<class MPTraits>
string
HPS<MPTraits>::
SelectNextSamplingMethod(bool _learning) {
  double proSum = 0;
  map<string, pair<double, double> > mapProRange;

  if(this->m_debug)
    cout << "\nmethod\tprob\trange_min\trange_max\n";
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
    double genProb = 0;
    if(_learning && m_samplerSelectionDistribution == "window_uniform")
      genProb = m_uniformProbability[*NG];
    else if(m_samplerSelectionDistribution == "nowindow")
      genProb = m_learnedProbability[*NG];
    else {
      genProb = m_nodeProbability[*NG];
    }
    double upperBound = 0;
    if(NG+1 != m_samplerLabels.end())
      upperBound = proSum + genProb; 
    else
      upperBound = 1.0;

    mapProRange[*NG] = make_pair(proSum, upperBound); 
    proSum += genProb;
   // cout<<"Probability Sum :"<<proSum<<endl;
 if(this->m_debug)
      cout << *NG << "\t" << genProb << "\t" << mapProRange[*NG].first << "\t" << mapProRange[*NG].second << endl;
  }


  if(!_learning && m_samplerSelectionDistribution == "window_hybrid_outsidewindow_highest")  {
    double maxProbability = 0;
    string maxGen = "";
    for(map<string, pair<double, double> >::const_iterator MPR = mapProRange.begin(); MPR != mapProRange.end(); ++MPR)
      if((MPR->second.second - MPR->second.first) > maxProbability) {
        maxProbability = MPR->second.second - MPR->second.first;
        maxGen = MPR->first;
      }
      if(this->m_debug)
    cout << "***\tHighest sampler after learning :: " << maxGen << endl;
    return maxGen;
  }
  else {
    double randomNum = DRand(); 
for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
      if(mapProRange[*NG].first <= randomNum && randomNum < mapProRange[*NG].second) {
        if(this->m_debug)
	cout << "***   The next node generator is::  " << *NG << endl;
        return *NG;
      }
   
    cerr << endl << endl << "This can't be good, exiting.";
    cerr << endl;
    for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
      cerr << *NG << ":: [" << mapProRange[*NG].first << ", " << mapProRange[*NG].second << "]" << endl;
    cerr << "Random Number:: " << randomNum << endl;
    exit(-1);
  }
}


template<class MPTraits>
double
HPS<MPTraits>::
ComputeVisibilityReward(double _samplertime, double _visibility, int _prevCcCount, int _currCcCount, Regions& _space)
{ 
  if(_currCcCount > _prevCcCount) {
    return 1.0;
  }
  else if(_currCcCount < _prevCcCount) {
    return 1.0;
  }
  else {
    return exp(- 0.1 *_visibility * _samplertime);
  }

}


template<class MPTraits>
bool
HPS<MPTraits>::
InLearningWindow(int _totalSamples) const {
  double d1 = _totalSamples % m_binSize;
  double d2 = double(m_binSize) * m_windowPercent;
  return d1 < d2;  //from the current values given in XML file, it will always return true.
}


template<class MPTraits>
void
HPS<MPTraits>::
RewardAndUpdateWeightsProbabilities(string _nodeSelected, double _reward, unsigned long int _cost) {
  int K = m_samplerLabels.size();

  //update costs:
  if(!m_fixedCost)
  {
    int numSampled = m_nodeNumSampled[_nodeSelected];
    int prevAvgCost = m_nodeCosts[_nodeSelected];
    int newAvgCost = (prevAvgCost * numSampled + _cost) / (numSampled + 1);
    m_nodeCosts[_nodeSelected] = newAvgCost;
  }


  if(this->m_debug)
  cout << "mpr: " << m_percentageRandom << " K: " << K << endl;
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG){
    double adjustedReward = 0.0;
    double new_weight=0.0;
    if(*NG == _nodeSelected) {
      adjustedReward = _reward / m_noCostProbability[*NG];
    if(this->m_debug)
    cout << "mngw: " << m_nodeWeights[*NG] << " ar: " << adjustedReward << ": " << *NG <<endl;
    new_weight = m_nodeWeights[*NG] * exp(double((m_percentageRandom) * adjustedReward / double(K)));
    m_nodeWeights[*NG] = new_weight;
  }
  }

  double weightTotal = 0;
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
    weightTotal += m_nodeWeights[*NG];
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
    m_noCostProbability[*NG] = (1 - m_percentageRandom) * (m_nodeWeights[*NG] / weightTotal) + (m_percentageRandom / K);
  }

  double probNoCostTotal = 0;
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
    probNoCostTotal += (m_noCostProbability[*NG] / double(m_nodeCosts[*NG]));
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG){
    if(!m_countCost) {
      m_nodeProbability[*NG] = m_noCostProbability[*NG];
    }
    else
      m_nodeProbability[*NG] = (m_noCostProbability[*NG] / double(m_nodeCosts[*NG])) / probNoCostTotal;
     
  }
      cout<<m_nodeProbability["OBPRM"]<<", "<<m_nodeProbability["Gauss"]<<", "<<m_nodeProbability["Bridge"]<<", "<<m_nodeProbability["UniformRandomFree"]<<endl;

  double smallestWeight = -1;
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG) {
    double weight = m_nodeWeights[*NG];
    if(weight < smallestWeight || smallestWeight == -1)
      smallestWeight = weight;
  }
  for(vector<string>::const_iterator NG = m_samplerLabels.begin(); NG != m_samplerLabels.end(); ++NG)
    m_nodeWeights[*NG] = m_nodeWeights[*NG] / smallestWeight;
}



#endif
