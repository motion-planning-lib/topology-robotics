#ifndef RRSE_H_
#define RRSE_H_

#include <stdint.h>

//assembly code to measure cpu cycles
/*static inline uint64_t GetCycles(){
  uint64_t n;
  __asm__ __volatile__ ("rdtsc" : "=A"(n));
  return n;
}*/

#include "BasicRRTStrategy.h"

template<class MPTraits>
class RRSE : public BasicRRTStrategy<MPTraits> {
  public:
    
    typedef typename MPTraits::CfgType CfgType;
    typedef typename MPTraits::WeightType WeightType;
    typedef typename MPTraits::MPProblemType MPProblemType;
    typedef typename MPProblemType::RoadmapType RoadmapType;
    typedef typename MPProblemType::GraphType GraphType;
    typedef typename MPProblemType::VID VID;

    //cost calculation for the RRSE
    enum CostMethod {/*FIXED,*/ REWARD, CYCLES};
    //map<string, pair<double, double> > is a single Growth Set representing
    //growth method, cost, weight tuple
    
    typedef map<VID, pair<pair<double, long>, double> > GrowthSet;
    //double is visibility threshold for the GrowthSet
    //typedef map<double, GrowthSet> GrowthSets;

    RRSE(double _wallPenalty = 0.5, double _gamma = 0.5,
        /*const GrowthSet& _growthSets = GrowthSet(),*/  CostMethod _c = REWARD);
    RRSE(MPProblemType* _problem, XMLNode& _node);

    virtual void Initialize();

    virtual void ParseXML(XMLNode& _node);
    virtual void Print(ostream& _os) const;

  protected:
    // Helper functions
    //virtual VID ExpandTree(CfgType& _dir);
    //string SelectGrowthMethod(GrowthSet& _gs);
    virtual VID ExpandTree(const VID _nearestVID, const CfgType& _target) override;
    void UpdateCost(double _cost, VID _wit, GrowthSet& _gs);
    void RewardGrowthNode(double _r, VID _wit, GrowthSet& _gs);
    VID UpdateTree(const VID _nearest, CfgType& _new, const CfgType& _dir, double _delta);
    VID UpdateTree(CfgType& _newCfg, const VID _nearVID, bool _againstWall,
        double _ratio);


  private:
    //This function incorporates _val into the running average of visibility in
    //_cfg
    void AvgVisibility(CfgType& _cfg, double _val);
    //simple helper to grab the visibility from _cfg
    double GetVisibility(CfgType& _cfg);
    double GetCost(VID _wit, GrowthSet& _gs);
    double CostInsensitiveProb(VID _wit, GrowthSet& _gs);

    double m_wallPenalty; //initial visibility for nodes which extend into C-obst
    double m_gamma;
    //double threshold;

    GrowthSet m_growthSets;

    CostMethod m_costMethod;
};

template<class MPTraits>
RRSE<MPTraits>::
RRSE(double _wallPenalty, double _gamma, /*const GrowthSet& _growthSets,*/
    CostMethod _c) : BasicRRTStrategy<MPTraits>(),
    m_wallPenalty(_wallPenalty), m_gamma(_gamma), /*m_growthSets(_growthSets),*/
    m_costMethod(_c) {
  this->SetName("RRSE");
}

template<class MPTraits>
RRSE<MPTraits>::
RRSE(MPProblemType* _problem, XMLNode& _node) :
    BasicRRTStrategy<MPTraits>(_problem, _node, false){
  this->SetName("RRSE");
  ParseXML(_node);
}

template<class MPTraits>
void
RRSE<MPTraits>::
ParseXML(XMLNode& _node) {
  /*for(auto& child : _node) {
    if(child.Name() == "GrowthSet"){
      double threshold = child.Read("threshold", true, 0.0, 0.0,
          1.0, "Threshold of visibility for selecting GrowthSet");
      GrowthSet growthSet;
      for(auto& child2 : child) {
        if(child2.Name() == "Extender"){
          string label = child2.Read("label", true, "", "Extender strategy");
          growthSet[label] = make_pair(make_pair(0, 0), 1.0);
        }
      }
      m_growthSets[threshold] = growthSet;
    }
  }*/

  /*threshold = _node.Read("threshold", true, 0.0, 0.0,
          1.0, "Threshold of visibility for selecting GrowthSet");*/
  m_wallPenalty = _node.Read("wallPenalty", false, 0.5, 0.0, 1.0,
      "Initial visibility for nodes agains C-obst");
  m_gamma = _node.Read("gamma", true, 0.5, 0.0, 1.0,
      "Gamma for adaptivity formulas");
  string costMethod = _node.Read("cost", true, "fixed", "Cost method");
  transform(costMethod.begin(), costMethod.end(),
      costMethod.begin(), ::tolower);

  //if(costMethod == "fixed")
    //m_costMethod = FIXED;
  //else 
  if(costMethod == "reward")
    m_costMethod = REWARD;
  else if(costMethod == "cycles")
    m_costMethod = CYCLES;
  else
    throw ParseException(_node.Where(), "Unknown cost method '" + costMethod +
        "'. Choices are 'fixed', 'reward', or 'cycles'.");
}

template<class MPTraits>
void
RRSE<MPTraits>::Print(ostream& _os) const {
  BasicRRTStrategy<MPTraits>::Print(_os);
  _os << "\tWallPenalty::" << m_wallPenalty << endl;
  _os << "\tGamma::" << m_gamma << endl;
  /*_os << "\tGrowthSet::" << endl;
  typedef typename GrowthSet::const_iterator GSIT;
  typedef typename GrowthSet::const_iterator GIT;
  for(GSIT gsit = m_growthSets.begin(); gsit!=m_growthSets.end(); ++gsit){
    _os << "\t\tGrowthSet::" << gsit->first << endl;
    for(GIT git = gsit->second.begin(); git!=gsit->second.end(); ++git){
      _os << "\t\t\t" << git->first << endl;
    }
  }*/
  _os << "\tCostMethod::";
  //if(m_costMethod == FIXED) _os << "fixed";
  //else 
  if(m_costMethod == REWARD) _os << "reward";
  else _os << "cycles";
  _os << endl;
}

template<class MPTraits>
void
RRSE<MPTraits>::Initialize(){
  BasicRRTStrategy<MPTraits>::Initialize();

  //for each root of the graph, make sure to initialize variables.
  GraphType* rdmp = this->GetRoadmap()->GetGraph();
  for(typename GraphType::VI v = rdmp->begin(); v!=rdmp->end(); v++){
    v->property().SetStat("Parent", 0);
    v->property().SetStat("Success", 1);
    v->property().SetStat("Fail", 0);
    v->property().SetStat("Visibility", 1);
  }
}

template<class MPTraits>
typename RRSE<MPTraits>::VID
RRSE<MPTraits>::ExpandTree(const VID _nearestVID, const CfgType& _target){

  // Setup MP Variables
  auto dm = this->GetDistanceMetric(this->m_dmLabel);
  VID recentVID = BasicRRTStrategy<MPTraits>::ExpandTree(_nearestVID, _target);

  //get the expand node from the roadmap
  /*auto nf = this->GetNeighborhoodFinder(this->m_nfLabel);
  vector<pair<VID, double> > kClosest;
  nf->FindNeighbors(this->GetRoadmap(),
      this->m_currentTree->begin(), this->m_currentTree->end(),
      this->m_currentTree->size() ==
      this->GetRoadmap()->GetGraph()->get_num_vertices(),
      _dir, back_inserter(kClosest));
  CfgType& nearest = this->GetRoadmap()->GetGraph()->GetVertex(kClosest[0].first);*/

  CfgType& nearest = this->GetRoadmap()->GetGraph()->GetVertex(_nearestVID);
  //get visibility of near node to decide which growth method to do
  double visibility = GetVisibility(nearest);
  
  //VID _nearest = this->GetRoadmap()->GetGraph()->GetVID(nearest);
  //cout<<"Node VID:"<<_nearest<<endl;
  //CfgType& nearest = this->GetRoadmap()->GetGraph()->GetVertex(_nearestVID);

  //vector<VID> _allnodes;
  //copy(this->m_currentTree->begin(), this->m_currentTree->end(), back_inserter(_allnodes));
  //CfgType node;
  //GrowthSet growth;

  pair<pair<double, long>, double> reward = make_pair(make_pair(0.0, 0), 1.0);
  m_growthSets[_nearestVID] = reward;

  /*for(typename vector<VID>::iterator i = _allnodes.begin(); i != _allnodes.end(); ++i){ 
       m_growthSets[*i] = reward;
  }*/

  if(this->m_debug){
    cout << "nearest:: " << nearest << "\tvisibility:: " << visibility << endl;
    cout<<"Node VID:"<<_nearestVID<<endl;
  }
  //cout<<"Radial Distance"<<endl;
  //minDist is taken as the radial distance for extension to happen.
  double minDist = this->GetExtender(this->m_exLabel)->GetMinDistance();

  if(dm->Distance(_target, nearest) < minDist){
    //chosen a q_rand which is too close. Penalize nearest with 0.
    nearest.IncStat("Fail");
    RewardGrowthNode(-minDist, _nearestVID, m_growthSets);
    AvgVisibility(nearest, 0);
    return INVALID_VID;
  }
 
  //select the growth set based upon the visibility of the nearest node
  //GrowthSet::reverse_iterator rgsit = m_growthSets.rbegin();
  /*for(; rgsit!=m_growthSets.rend(); ++rgsit){
    if(visibility > rgsit->first){
      break;
    }
  }*/

  //select the growth method from the selected growth set
 // string gm = SelectGrowthMethod(rgsit->second);

  //grow the RRT using growth method gm
  //start timing from cycles
  //uint64_t start = GetCycles();
  
  //cout<<"New Configuration taken"<<endl;
  //VID newVID = BasicRRTStrategy<MPTraits>::ExpandTree(_nearestVID, _target);
  //cout<<"configuration taken"<<endl;
  CfgType& newCfg = this->GetRoadmap()->GetGraph()->GetVertex(recentVID);
  //LPOutput<MPTraits> lpOutput;
  //auto e = this->GetExtender(this->m_exLabel);
  //bool verifiedValid = e->Extend(nearest, _dir, newCfg, lpOutput);
  double delta = this->GetExtender(this->m_exLabel)->GetMaxDistance();

  //end timing from cycles
  //uint64_t end = GetCycles();
  //uint64_t cost = end - start;

  //update cost based upon cycles
  //if(m_costMethod == CYCLES)
    //UpdateCost(cost, _nearestVID, m_growthSets);

  //expansion Failed Penalize nearest with 0
  /*if(!verifiedValid) {
    if(this->m_debug)
      cout << "Growth Failed on::" << nearest << ", with visibility::"
           << visibility << endl;
    nearest.IncStat("Fail");
    AvgVisibility(nearest, 0);

    if(m_costMethod == REWARD)
      UpdateCost(delta, _nearestVID, m_growthSets);

    //reward the growth strategy based upon expanded distance in proportion to
    //delta_q
    RewardGrowthNode(-minDist, _nearestVID, m_growthSets);

    return recentVID;
  }*/
  double exetime = 0.0;
  cout<<"Checking..."<<endl;
  if(recentVID != INVALID_VID){
    // If good to go, add to roadmap
    StatClass* stats = this->GetMPProblem()->GetStatClass();
    stats->StartClock("Total time");

    double dist = dm->Distance(newCfg, nearest);

    if(m_costMethod == REWARD)
      UpdateCost(max(delta - dist, 0.0) + 1E-6, _nearestVID, m_growthSets);

    if(dist >= minDist) {
    //expansion success
      nearest.IncStat("Success");
    //update the tree
    //Generate Waypoints is from AdaptiveMultiResRRT, but this one does not
    //acutally add nodes.
    recentVID = UpdateTree(_nearestVID, newCfg, _target, delta);
    //if(recentVID > this->m_currentTree->back()) {
      //this->m_currentTree->push_back(recentVID);
      //reward the growth strategy based upon expanded distance in proportion to
      //delta_q
      RewardGrowthNode(dist/delta, _nearestVID, m_growthSets);
      
      stats->StopClock("Total time");
      exetime = stats->GetSeconds("Total time");
      cout<<"Update time for Witness node :"<< exetime <<endl;
      
     }
     else {
      nearest.IncStat("Fail");
      AvgVisibility(nearest, 0);  
      RewardGrowthNode(-minDist, _nearestVID, m_growthSets);
    }
  }
 /*else{
    //could not expand to new node from witness node, Penalize the witness node;
    nearest.IncStat("Fail");
    AvgVisibility(nearest, 0);
    RewardGrowthNode(-minDist, _nearestVID, m_growthSets);
    cout<<"Node Penalized"<<endl;
  }*/
  cout<<"Done:"<<endl;
  if(this->m_debug)
  cout<<"\n Witness Node :"<<_nearestVID<<"\t Cost :"<< m_growthSets[_nearestVID].first.first<<"\t Reward :"<< m_growthSets[_nearestVID].second<< endl;
  
  return recentVID;
  //return BasicRRTStrategy<MPTraits>::ExpandTree(_nearestVID, _target);
}

/*
template<class MPTraits>
string
RRSE<MPTraits>::SelectGrowthMethod(GrowthSet& _gs){
  if(this->m_debug)
    cout << ":::::Selecting Growth Method:::::" << endl;
  map<string, double> pistars, pis;
  double spc = 0.0;
  //compute p_i, c_i and the sum over all growth methods
  for(GrowthSet::const_iterator gsit = _gs.begin(); gsit!=_gs.end(); ++gsit){
    if(this->m_debug)
      cout << "Method::" << gsit->first
        << "::Cost::" << GetCost(gsit->first, _gs)
        << "::Weight::" << gsit->second.second << endl;
    double pistar = CostInsensitiveProb(gsit->first, _gs)/GetCost(gsit->first,
        _gs);
    pistars[gsit->first] = pistar;
    spc += pistar;
  }

  //compute p_i for each method
  for(GrowthSet::const_iterator gsit = _gs.begin(); gsit!=_gs.end(); ++gsit){
    pis[gsit->first] = pistars[gsit->first]/spc;
    if(this->m_debug)
      cout << "Method::" << gsit->first
        << "::Prob::" << pis[gsit->first] << endl;
  }

  if(this->m_debug)
    cout << endl << endl;

  //select method based upon probability
  double r = DRand(), cumm = 0.0;
  for(map<string, double>::const_iterator mit = pis.begin(); mit!=pis.end();
      ++mit){
    cumm += mit->second;
    if(r <= cumm) {
      if(this->m_debug) {
        cout << "r::" << r << endl;
        cout << "MethodSelected::" << mit->first << endl;
      }
      return mit->first;
    }
  }

  cerr << "Error::Growth method not selected." << endl;
  exit(1);
}*/

template<class MPTraits>
void
RRSE<MPTraits>::UpdateCost(double _cost, VID _wit, GrowthSet& _gs){
  //update the average cost of the growth method
  if(_gs[_wit].first.second == 0){
    _gs[_wit].first.second = 1;
    _gs[_wit].first.first = _cost;
  }
  else{
    _gs[_wit].first.first += _cost;
    _gs[_wit].first.second++;
  }
}

template<class MPTraits>
void
RRSE<MPTraits>::RewardGrowthNode(double _r, VID _wit, GrowthSet& _gs){
  if(this->m_debug)
    cout << "Reward::" << "::" << _r << endl;
  //factor is e^(g * x_i' / K) where g is gamma, x_i' = x_i/p_i* where x_i is
  //the reward r, and K is the number of growth methods
  double factor = exp(m_gamma * (_r/CostInsensitiveProb(_wit, _gs)) /
      double(_gs.size()));
  //update the weight on growth method _s
  _gs[_wit].second *= factor;
}

//This function simply calls the correct update tree based on expansion
//type.
template<class MPTraits>
typename RRSE<MPTraits>::VID
RRSE<MPTraits>::UpdateTree(const VID _expandNode, CfgType& _newCfg,
    const CfgType& _dir, double _delta){
  auto dm = this->GetDistanceMetric(this->m_dmLabel);

  CfgType& nearest = this->GetRoadmap()->GetGraph()->GetVertex(_expandNode);

  double visibility = GetVisibility(nearest);
  double distToNear = dm->Distance(nearest, _newCfg);
  //if expansion did not reach at least delta * visibility and q_new is not
  //q_rand. Then it will be a partial expansion
  bool partial = distToNear < _delta && _newCfg != _dir;
  double ratio = distToNear / _delta;

  VID returnVID = UpdateTree(_newCfg, _expandNode, partial, ratio);

  if(this->m_debug){
    cout << "near vid::" << _expandNode << "\tvisibility::" << visibility
         << endl;
    cout << "new vid::" << returnVID << "\tvisibility::"
         << GetVisibility(_newCfg) << endl;
    cout << "expansionRatio::" << ratio << endl;
  }

  return returnVID;
}

//add node to tree, and update visibility
template<class MPTraits>
typename RRSE<MPTraits>::VID
RRSE<MPTraits>::UpdateTree(CfgType& _newCfg, const VID _nearVID, bool _againstWall, double _ratio){

  GraphType* rdmp = this->GetRoadmap()->GetGraph();

  //add the vertex to the graph
  VID newVID = rdmp->AddVertex(_newCfg);

  //calculate edge weight and add edge
  CfgType& parentcfg = rdmp->GetVertex(_nearVID);
  if(this->m_debug)
   cout << "parentcfg::" << parentcfg << endl;
  int weight;
  CfgType incr;
  Environment* env = this->GetEnvironment();
  incr.FindIncrement(parentcfg, _newCfg, &weight, env->GetPositionRes(),
      env->GetOrientationRes());
  pair<WeightType, WeightType> weights = make_pair(WeightType("RRTExpand",
      weight), WeightType("RRTExpand", weight));
  rdmp->AddEdge(_nearVID, newVID, weights);

  //update the visibility at the new node and near node
  CfgType& newcfg = rdmp->GetVertex(newVID);
  newcfg.SetStat("Parent", _nearVID);

  //The new node is against a wall. Set visibility of new node to wall penalty.
  //Update parent with expansion ratio
  if(_againstWall){
    newcfg.SetStat("Visibility", m_wallPenalty);
    AvgVisibility(parentcfg, _ratio);
  }
  //newCfg inherits half of parents visibility and half of the ratio currently
  //parent is updated with ratio (mostly 1 in this case)
  else{
    newcfg.SetStat("Visibility", (_ratio + parentcfg.GetStat("Visibility"))/
        2.0);
    AvgVisibility(parentcfg, _ratio);
  }

  //intialize success and fail
  newcfg.SetStat("Success", 1);
  newcfg.SetStat("Fail", 0);
  VDAddTempEdge(parentcfg, _newCfg);

  return newVID;
}

//note:: this function assumes that the total attempts has already been updated
//which is why we multiply by (total-1) and divide by (total) instead of (total)
//and (total+1)
template<class MPTraits>
void
RRSE<MPTraits>::AvgVisibility(CfgType& _cfg, double _val){
  double success = _cfg.GetStat("Success");
  double fail = _cfg.GetStat("Fail");
  double total = success+fail;
  _cfg.SetStat("Visibility", (_cfg.GetStat("Visibility")*(total-1) + _val)/
      total);
}

//Visibility is stored with the cfg. Isolate definition of visibility to this
//function.
template<class MPTraits>
double
RRSE<MPTraits>::GetVisibility(CfgType& _cfg){
  if(!_cfg.IsStat("Visibility")){
    cerr << "Warning::Visibility not a statistic of node::" << _cfg << endl;
    cerr << "Setting and Returning 1 as its visibility." << endl;
    _cfg.SetStat("Success", 1);
    _cfg.SetStat("Fail", 0);
    _cfg.SetStat("Visibility", 1);
  }
  return _cfg.GetStat("Visibility");
}

template<class MPTraits>
double
RRSE<MPTraits>::GetCost(VID _wit, GrowthSet& _gs){
  //if(m_costMethod == FIXED)
    //return 1;
  if(_gs[_wit].first.second == 0)
    return 1;
  else
    return _gs[_wit].first.first/(double)_gs[_wit].first.second;
}

template<class MPTraits>
double
RRSE<MPTraits>::CostInsensitiveProb(VID _wit, GrowthSet& _gs){
  double sw = 0.0;
  for(typename GrowthSet::const_iterator gsit = _gs.begin(); gsit!=_gs.end(); ++gsit){
    sw+=log(gsit->second.second+1);
  }
  return (1.-m_gamma)*log(_gs[_wit].second+1)/sw + m_gamma*double(_gs.size());
}

#endif
