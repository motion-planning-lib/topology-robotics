#ifndef VRINCREMENT_PLANNER_H_
#define VRINCREMENT_PLANNER_H_

#include "Environment/Boundary.h"
#include "Environment/BoundingSphere.h"
#include "Environment/Environment.h"
#include "Environment/Body.h"
#include "Environment/FixedBody.h"
#include "MPStrategyMethod.h"
#include "Utilities/MedialAxisUtilities.h"

#define PI 3.14159265


template<class MPTraits>
class VRIncremental : public MPStrategyMethod<MPTraits> {
	public:
		typedef typename MPTraits::CfgType CfgType;
		typedef typename MPTraits::MPProblemType MPProblemType;
		typedef typename MPProblemType::RoadmapType RoadmapType;
		typedef typename RoadmapType::GraphType GraphType;
		typedef typename MPProblemType::SamplerPointer SamplerPointer;
		typedef typename MPProblemType::ConnectorPointer ConnectorPointer;
		typedef typename MPProblemType::VID         VID;
		typedef typename MPProblemType::NeighborhoodFinderPointer NeighborhoodFinderPointer;
		typedef typename MPProblemType::MapEvaluatorPointer MapEvaluatorPointer;

		typedef vector<VID>                         TreeType;

		VRIncremental(string _dm = "euclidean", string _nf = "bfnf",
				string _vc = "rapid", string _nc = "kClosest", 
				vector<string> _evaluatorLabels = vector<string>());

		VRIncremental(MPProblemType* _problem, XMLNode& _node);

		virtual ~VRIncremental() {}

		virtual void ParseXML(XMLNode& _node);
		virtual void Print(ostream& _os) const;

		virtual void Initialize();
		virtual void Run();
		virtual void Finalize();

		// Helper Functions

		////////////////////////////////////////////////////////////////////////////
		/// \brief Get a random configuration to grow towards.
		virtual CfgType SelectQRand();

		////////////////////////////////////////////////////////////////////////////
		/// \brief Expands the voxel tree to new random node until connected t goal.
		/// \Input target - goal configuration.
		/// \Output newCfg - returns a new random node
		bool ExtendTree(CfgType& target);

		////////////////////////////////////////////////////////////////////////////
		/// \brief sample within the bounding sphere voxel.
		/// \Input rdmNode - a extension configuration of the voxel tree.
		/// \Output set of nodes sampled in the smaller region, i.e. bounding sphere.
		bool SampleInVoxel(vector<CfgType>& expNodes);

		///////////////////////////////////////////////////////////////////////////
		/// \brief Perform simplicial collapse on the constructed simplicial complex.
		/// \Input set of generated sample nodes within voxel region.
		/// \Output set of preserved topological nodes.
		vector<VID> Collapse(vector<VID>& regionID);

		///////////////////////////////////////////////////////////////////////////
		/// \brief Represent the binary form of each clique and perform symmetric difference.
		/// \Input cliques set, total nodes in current voxel.
		/// \return maximal clique nodes.
		vector<VID> BinaryRep(vector<vector<VID>>& cliques, int size);

		///////////////////////////////////////////////////////////////////////////
		/// \brief Computes Morse values and identify critical points and feasible critical points.
		/// \Input set of sampled nodes within a smaller region.
		/// \return set of identified feasible critical points.
		vector<vector<VID>> MorseFnc(vector<VID>& voxelVID);

		//get the obstacle points
		set<Point3d> GetObstaclePt();

		///////////////////////////////////////////////////////////////////////////
		/// \brief Calculates the minimum distance between the samples and the obstacles.
		/// \Input set of samples and set of obstacle points.
		/// \return distance information of samples from each obstacle.
		map<Point3d, double> MinDisttoObstacle(vector<Point3d>& SamplePoints, set<Point3d>& boundaryPoints);

		///////////////////////////////////////////////////////////////////////////
		/// \brief [Density function] Counts the neighbors around each obstacle point.
		/// \Input set of samples and set of obstacle points
		/// \return set of neighbors around each obstacle point.
		map<Point3d, int> DensityMethod(set<Point3d>& boundaryPoints, vector<VID>& voxelVID, map<Point3d, vector<VID>>& neighborPt);

		///////////////////////////////////////////////////////////////////////////
		/// \brief Updates tree using extracted topological and geometrical configurations.
		/// \Input set of extracted topological and geometrical nodes.
		void TopologyTree(vector<VID>& vrNodes);


	protected:
		string m_samplerLabel;  ///< The sampler label
		string m_dmLabel;       ///< The distance metric label.
		string m_nfLabel;       ///< The neighborhood finder label.
		string m_vcLabel;       ///< The validity checker label.
		string m_ncLabel;       ///< The connector label.
		CfgType extNode;        ///< The extending node to the goal.

		vector<Point3d> samplePt; //< stores sub-region configurations. 
		map<Point3d, vector<VID>> neighborPt; //< records feasible critical points.
		map<Point3d, CfgType> nodeMap; //< maps the cfg to its 3D cartesian coordinate.

		// Voxel Data Structure
		vector<TreeType> m_vpoly; ///< stores nodes of all voxel set.
		//TreeIter m_currentTree; ///< The working tree.
		vector<VID> m_voxel; /// The current voxel preserved nodes.

		PathQuery<MPTraits>* m_query{nullptr}; ///< The query object.
};

template<class MPTraits>
VRIncremental<MPTraits>::
VRIncremental(string _dm, string _nf, string _vc, string _nc, /*string _ex,*/ 
		const vector<string> _evaluatorLabels):
	m_dmLabel(_dm), m_nfLabel(_nf), m_vcLabel(_vc), m_ncLabel(_nc)/*, m_exLabel(_ex)*/ {

		this->m_meLabels = _evaluatorLabels;
		this->SetName("VRIncremental");
	}

template<class MPTraits>
VRIncremental<MPTraits>::
VRIncremental(MPProblemType* _problem, XMLNode& _node) :
	MPStrategyMethod<MPTraits>(_problem, _node) {
		this->SetName("VRIncremental");
		ParseXML(_node);
	}

template<class MPTraits>
void
VRIncremental<MPTraits>::
ParseXML(XMLNode& _node) {
	// Parse MP object labels
	m_vcLabel = _node.Read("vcLabel", true, "", "Validity Test Method");
	m_nfLabel = _node.Read("nfLabel", true, "", "Neighborhood Finder");
	m_dmLabel = _node.Read("dmLabel",true,"","Distance Metric");
	m_ncLabel = _node.Read("connectorLabel", false, "", "Node Connection Method");

	for(auto& child : _node) {
		if(child.Name() == "Sampler") {
			m_samplerLabel = child.Read("method", true, "", "Node Generation Method");
		}
		if(child.Name() == "Evaluator") {
			this->m_meLabels.push_back(child.Read("method", true, "", "Evaluation Method"));
		}
	}
}

template<class MPTraits>
void
VRIncremental<MPTraits>::
Print(ostream& _os) const {
	MPStrategyMethod<MPTraits>::Print(_os);
	_os << "VRIncremental::Print" << endl
		<< "  MP objects:" << endl
		<< "\tDistance Metric:: " << m_dmLabel << endl
		<< "\tNeighborhood Finder:: " << m_nfLabel << endl
		<< "\tValidity Checker:: " << m_vcLabel << endl
		<< "\tConnection Method:: " << m_ncLabel << endl;

	_os << "\tSampling method: " << m_samplerLabel << endl;

	_os<<"\tMapEvaluator: ";
	for(const auto& label : this->m_meLabels)
		_os << "\t" << label << endl;
}

template<class MPTraits>
void VRIncremental<MPTraits>::Initialize(){
	if(this->m_debug)
		cout << "Initializing Incremental Planner" << endl;

	// Clear all state variables to avoid problems when running multiple times.
	m_vpoly.clear();
	GraphType* g = this->GetRoadmap()->GetGraph();

	// Check for query info.
	m_query = nullptr;

	// If a query was loaded, process query cfgs
	if(this->UsingQuery()) {
		m_query = static_cast<PathQuery<MPTraits>*>(this->GetMapEvaluator("PathQuery").
				get());
		const vector<CfgType>& queryCfgs = m_query->GetQuery();
		cout<<"o/p query: "<<queryCfgs.size()<<endl;

		// If growing goals, set each query cfg as its own tree
		if(queryCfgs.size() > 1 /*m_growGoals*/) {
			for(auto& cfg : queryCfgs) {
				VID add = g->AddVertex(cfg);
				m_vpoly.push_back(vector<VID>(1, add));
			}
		}

		// If not growing goals, add only the start to map.
		else {
			VID start = g->AddVertex(queryCfgs.front());
			m_vpoly.push_back(vector<VID>(1, start));
		}
	}
	else {
		auto env = this->GetEnvironment();
		auto vc  = this->GetValidityChecker(m_vcLabel);

		CfgType root;
		do {
			root.GetRandomCfg(env);
		} while(!env->InBounds(root) || !vc->IsValid(root, "VRIncremental"));

		VID rootVID = g->AddVertex(root);
		m_vpoly.push_back(vector<VID>(1, rootVID));
	}


	// Output debugging info if requested
	if(this->m_debug) {
		cout << "There are " << m_vpoly.size() << " trees"
			<< (m_vpoly.empty() ? "." : ":") << endl;
		for(size_t i = 0; i < m_vpoly.size(); ++i) {
			cout << "\tTree " << i << " has " << m_vpoly[i].size() << " vertices.\n";
			if(!m_vpoly[i].empty())
				cout << "\t\tIts root is: " << g->GetVertex(m_vpoly[i].front()) << endl;
		}
	}



}


template<class MPTraits>
void VRIncremental<MPTraits>::Run(){

	bool mapPassedEvaluation = this->EvaluateMap();
	//RoadmapType* rmap = this->GetMPProblem()->GetRoadmap();
	auto boundary = this->GetEnvironment()->GetBoundary();
	vector<VID> regionID, vrNodes;
	vector<vector<VID>> dmt;


	// Find growth direction. Default is to randomly select node or bias
	// towards a goal.
	CfgType target;
	if(m_query && !m_query->GetGoals().empty()) {
		target = m_query->GetRandomGoal();
		if(this->m_debug)
			cout << "Goal-biased direction selected: " << target << endl;
	}
	else {
		target = this->SelectQRand();
		if(this->m_debug)
			cout << "Random direction selected: " << target << endl;
	}
	StatClass* stats = this->GetStatClass();
	string clockName = "Voxel time";

	while(!mapPassedEvaluation){
		cout<<"Evaluating Query"<<endl;
		stats->StartClock(clockName);

		if(ExtendTree(target)){
			GraphType* g = this->GetRoadmap()->GetGraph();
			cout<<"Total Nodes: "<<g->get_num_vertices()<<endl;

			this->GetConnector(m_ncLabel)->Connect(this->GetRoadmap(), g->begin(), g->end());

			vector<pair<size_t, VID>> ccs;
			stapl::sequential::vector_property_map<GraphType, size_t> cmap;
			get_cc_stats(*g, cmap, ccs);

			cout<<"Total ccs: "<<ccs.size()<<endl;

			stats->StopClock(clockName);

			cout<<"query pass1: "<< mapPassedEvaluation << endl;
			mapPassedEvaluation = this->EvaluateMap();
			cout<<"query pass2: "<< mapPassedEvaluation << endl;

		}
		else{
			cout<<"The expansion failed, so no path generated"<<endl;
			break;
		}

	}

}

template<class MPTraits>
typename MPTraits::CfgType
VRIncremental<MPTraits>::
SelectQRand() {
	try{
		CfgType mySample;
		mySample.GetRandomCfg(this->GetEnvironment(), this->GetEnvironment()->GetBoundary());
		return mySample;
	}
	//catch Boundary too small exception
	catch(PMPLException& _e) {
		CfgType mySample;
		mySample.GetRandomCfg(this->GetEnvironment());
		return mySample;
	}
	//catch all others and exit
	catch(exception& _e) {
		cerr << _e.what() << endl;
		exit(1);
	}

}

template<class MPTraits>
bool
VRIncremental<MPTraits>::
ExtendTree(CfgType& target){

	//finding nearest node from HULL(voxel) to the goal
	GraphType* g = this->GetRoadmap()->GetGraph();
	auto dm = this->GetDistanceMetric(m_dmLabel);

	vector<CfgType> expand;

	if(g->get_num_vertices() > 2 && samplePt.size() > 0){
		set<double> dist;
		double min = 0.0, median = 0.0, expansion = 0.0;
		CfgType incr;
		int nticks;

		incr.FindIncrement(extNode, target, &nticks, 
				this->GetEnvironment()->GetPositionRes(), 
				this->GetEnvironment()->GetOrientationRes());

		expansion = fabs(dm->Distance(extNode, target) - 
				4 * (extNode.DOF()) * (this->GetEnvironment()->GetRobot(0)->GetBoundingSphereRadius())); 
		//expansion = dm->Distance(extNode, target)/2;
		if(this->m_debug)
			cout<<"closest distance "<< expansion <<endl;

		while(dm->Distance(extNode, target) > expansion){
			extNode.IncrementTowardsGoal(target, incr);
		} 
		//cout<<"witness node "<<extNode<<endl;

		for(auto ptr = samplePt.begin(); ptr != samplePt.end(); ptr++){
			CfgType node = nodeMap[*ptr];
			dist.insert(dm->Distance(node, extNode));
		}

		if(!dist.empty()){
			min = *(dist.begin());

			if(dist.size() % 2 == 0)
			{
				auto lo = next(dist.begin(), dist.size()/ 2 - 1);
				auto hi = next(lo);
				median = (*lo + *hi) / 2;
			}
			else
				median = *next(dist.begin(), dist.size()/ 2);
		}
		//cout<<"median: "<<median<<endl;

		for(auto ptr = samplePt.begin(); ptr != samplePt.end(); ptr++){
			CfgType cfg = nodeMap[*ptr];

			if(min == dm->Distance(cfg, extNode)){
				if(this->GetValidityChecker(m_vcLabel)->IsValid(cfg, "VRIncremental"))
					expand.push_back(cfg);
			}
			else if(min < dm->Distance(cfg, extNode) && dm->Distance(cfg, extNode) < median){
				if(this->GetValidityChecker(m_vcLabel)->IsValid(cfg, "VRIncremental"))
					expand.push_back(cfg);
			}

		}
		dist.clear();

	}
	else {
		expand.push_back(g->GetVertex(m_vpoly[0].front()));
	}

	nodeMap.clear();

	return SampleInVoxel(expand);

}

template<class MPTraits>
bool
VRIncremental<MPTraits>::
SampleInVoxel(vector<CfgType>& expNodes){

	//setting a voxel polytope around the robot
	CfgType rdmNode;
	if(expNodes.size() > 2){
		srand((unsigned) time(0));
		int random = rand() % expNodes.size();
		rdmNode = expNodes[random];
	}
	else
		rdmNode = expNodes.front();

	Vector3d center = rdmNode.GetPoint();
	int n = rdmNode.DOF();
	double d = 2 * n * (this->GetEnvironment()->GetRobot(0)->GetBoundingSphereRadius());
	cout<<"sub-region radius: "<< d/n<<endl;
	shared_ptr<Boundary> Bbox(new BoundingSphere(center, d));

	vector<CfgType> vectorCfgs;
	vector<VID> voxelVID;

	SamplerPointer sampler = this->GetSampler(m_samplerLabel);
	sampler->Sample(15, 1, Bbox, back_inserter(vectorCfgs));

	if(vectorCfgs.size() > 5){
		for(typename vector<CfgType>::iterator C = vectorCfgs.begin(); C != vectorCfgs.end(); ++C) {
			if(C->IsLabel("VALID") && C->GetLabel("VALID")) {
				VID newVID = this->GetRoadmap()->GetGraph()->AddVertex(*C);
				voxelVID.push_back(newVID);

				if(this->m_debug) {
					cout<<"Node vertex ID "<< newVID <<endl;
				}

				const Point3d& _s = (*C).GetPoint();
				samplePt.push_back(_s);
				nodeMap[_s] = *C;

			}
		}
		cout<<"The expanding node is "<<rdmNode<<endl;
		extNode = rdmNode;
		/*if(this->GetValidityChecker(m_vcLabel)->IsValid(rdmNode, "VRIncremental")){
		  voxelVID.push_back(this->GetRoadmap()->GetGraph()->AddVertex(rdmNode));
		  }*/
		TopologyTree(voxelVID);
		return true;
	}
	else {
		vectorCfgs.clear();
		remove(expNodes.begin(), expNodes.end(), rdmNode);
		if(!expNodes.empty())
			SampleInVoxel(expNodes);
	}

	return false;

}

template<class MPTraits>
vector<typename VRIncremental<MPTraits>::VID>
VRIncremental<MPTraits>::
Collapse(vector<VID>& regionID){

	//Connecting the samples of the sub-region
	vector<vector<VID>> cliques; 

	// Get cc info from roadmap
	GraphType* g = this->GetRoadmap()->GetGraph();
	// get new graph with only currrent voxel nodes
	GraphType* t_graph = (new RoadmapType())->GetGraph();
	VID node;

	if(!regionID.empty()){
		for(auto tr = regionID.begin(); tr != regionID.end(); ++tr) {
			node = t_graph->AddVertex(g->GetVertex(*tr));
			cout<<"VID: "<<node<<endl;
		}
	}

	this->GetConnector(m_ncLabel)->Connect(this->GetRoadmap(), t_graph->begin(), t_graph->end());
	vector<pair<size_t, VID>> ccs;
	stapl::sequential::vector_property_map<GraphType, size_t> cmap;
	get_cc_stats(*t_graph, cmap, ccs);
	cliques.clear();
	//cliques.resize(ccs.size());

	if(ccs.size() > 0){
		//Iterate through each list from cc info
		vector<VID> ccVIDs;
		for(auto& cc : ccs) {
			cmap.reset();
			ccVIDs.clear();
			get_cc(*t_graph, cmap, cc.second, ccVIDs);
			cliques.push_back(ccVIDs);
		}
	}
	cout<<"recorded cliques: "<<ccs.size()<<endl;

	vector<VID> preserveID = BinaryRep(cliques, regionID.size());
	// sets size of each clique as total nodes and rep. binary form for nodes in each clique. 
	// perform symmetric diff. and o/p preserved nodes.

	vector<VID> graphNodes;
	for(typename vector<VID>::iterator nit = preserveID.begin(); nit != preserveID.end(); ++nit){
		graphNodes.push_back(g->AddVertex(t_graph->GetVertex(*nit)));
	}

	return graphNodes;

}

template<class MPTraits>
vector<typename VRIncremental<MPTraits>::VID>
VRIncremental<MPTraits>::BinaryRep(vector<vector<VID>>& cliques, int size){
	cout<<"Inside binary rep"<<endl;
	int nodes = size; //this->GetRoadmap()->GetGraph()->get_num_vertices();
	vector<vector<bool>> bits;
	int bitIndex = 0;
	ostream_iterator<bool> output(cout, "");

	for(typename vector<vector<VID>>::iterator it = cliques.begin(); it != cliques.end(); ++it){
		vector<bool> biNodes(nodes, false);
		for(typename vector<VID>::iterator wit = (*it).begin(); wit != (*it).end(); ++wit){
			int val = (*wit);
			for(int i = 0; i < nodes; i++){
				if(val == i)
					biNodes[i] = 1;
			}
		}

		bits.push_back(biNodes);
		copy(bits[bitIndex].begin(), bits[bitIndex].end(), output);
		cout<<endl;
		bitIndex++;
	}

	vector<bool> result(nodes, false), sym(nodes, false);
	//Do bitwise XOR
	for(int k = 0; k < bitIndex; k++){
		sym = bits[k];
		for(int z = 0; z < nodes; z++){
			result[z] = result[z] ^ sym[z];
		}
		sym.clear();
	}

	ostringstream s;
	copy(result.begin(), result.end(), ostream_iterator<bool>(s,""));
	string out = s.str();
	vector<VID> impNodes;
	cout<<"preserved nodes: ";

	for(size_t n = 0; n < out.length(); n++) {
		if(out[n] == '1') {
			impNodes.push_back(n);
			cout<< n <<" ";
		}
	}
	cout<<endl;

	return impNodes;
}

template<class MPTraits>
vector<vector<typename VRIncremental<MPTraits>::VID>>
VRIncremental<MPTraits>:: MorseFnc(vector<VID>& voxelVID){
	set<Point3d> ObstaclePt = GetObstaclePt();
	map<Point3d, double> dist = MinDisttoObstacle(samplePt, ObstaclePt);
	map<Point3d, int> dense = DensityMethod(ObstaclePt, voxelVID, neighborPt);
	map<Point3d, double> morse;

	for(typename set<Point3d>::iterator oit = ObstaclePt.begin(); oit != ObstaclePt.end(); ++oit){
		double m = dist[*oit] * dense[*oit];
		cout<<"Morse Values:"<<m<<endl;
		morse[*oit] = m;
	}
	cout<<"fcp: "<<neighborPt.size()<<endl;

	vector<vector<VID>> fcpt;
	for(typename map<Point3d, double>::iterator mit = morse.begin(); mit != morse.end(); ++mit){
		if(mit->second != 0){
			fcpt.push_back(neighborPt[mit->first]);
		}
	}
	neighborPt.clear();

	return fcpt;
}

template<class MPTraits>
set<Point3d>
VRIncremental<MPTraits>::GetObstaclePt(){
	set<Point3d> ObstaclePts;

	for(size_t i = 0; i < this->GetEnvironment()->NumObstacles(); ++i) {
		GMSPolyhedron& polyhedron = this->GetEnvironment()->GetObstacle(i)->GetFixedBody(0)->GetWorldPolyhedron();
		vector<Vector3d> vertexList = polyhedron.GetVertexList();

		for(typename vector<Vector3d>::iterator lit = vertexList.begin(); lit != vertexList.end(); ++lit) {
			ObstaclePts.insert(*lit);
		}

	}
	return ObstaclePts;
}

//Computes min. distance between the critical points and simplicial complex.
template<class MPTraits>
map<Point3d, double>
VRIncremental<MPTraits>::
MinDisttoObstacle(vector<Point3d>& SamplePoints, set<Point3d>& boundaryPoints){
	map<Point3d, double> mindist;

	for(typename set<Point3d>::iterator git = boundaryPoints.begin(); git != boundaryPoints.end(); ++git) {
		double minima = INFINITY;
		for(typename vector<Point3d>::iterator it = SamplePoints.begin(); it != SamplePoints.end(); ++it) {
			const Vector3d sub = (*git) - (*it);
			double d3 = sub.norm();

			if(minima > d3){
				minima = d3;
			}
		}

		mindist.insert({*git, minima});
	}

	return mindist;

}

//Counts no. of nodes in simplicial complex at clearance varrho from obstacles.
template<class MPTraits>
map<Point3d, int>
VRIncremental<MPTraits>::
DensityMethod(set<Point3d>& boundaryPoints, vector<VID>& voxelVID, map<Point3d, vector<VID>>& neighborPt){
	//auto nf = this->GetNeighborhoodFinder(this->m_nfLabel);
	auto nf = this->GetNeighborhoodFinder("RadiusNF");
	map<Point3d, int> density;

	for(typename set<Point3d>::iterator ait = boundaryPoints.begin(); ait != boundaryPoints.end(); ++ait) {
		CfgType cfg(*ait, size_t(-1));
		int d = 0;

		vector<pair<VID, double>> neighbors;

		nf->FindNeighbors(this->GetMPProblem()->GetRoadmap(), voxelVID.begin(),
				voxelVID.end(), false, cfg, back_inserter(neighbors));

		if(neighbors.empty())
			continue;

		for(const auto& nb : neighbors){
			neighborPt[*ait].push_back(nb.first);
		}
		d = (neighborPt[*ait]).size();

		cout<<*ait<<"density:"<<d<<endl;
		density.insert({*ait, d});

	}

	return density;
}

template<class MPTraits>
void
VRIncremental<MPTraits>::
TopologyTree(vector<VID>& vrNodes){
	cout<<"Updating tree"<<endl;

	vector<VID> clpNd = Collapse(vrNodes);
	vector<vector<VID>> dmt = MorseFnc(vrNodes);
	m_voxel.clear();

	//Insert feasible points to the tree 
	if(!dmt.empty()){
		for(typename vector<vector<VID>>::iterator rit = dmt.begin(); rit != dmt.end(); ++rit){
			for(typename vector<VID>::iterator yit = (*rit).begin(); yit != (*rit).end(); ++yit){
				if(find(clpNd.begin(), clpNd.end(), (*yit)) == clpNd.end()){
					clpNd.push_back((*yit));
					cout<<"dmt:"<<*yit<<endl;
				}
			}
		}
	}
	//append topological nodes to the tree
	copy(clpNd.begin(), clpNd.end(), back_inserter(m_voxel));
	m_vpoly.push_back(m_voxel);

	cout<<"Voxel Nodes :"<<m_voxel.size()<<endl;
	cout<<"No. of voxels: "<<m_vpoly.size()<<endl; 

	samplePt.clear();
	for(typename vector<VID>::iterator cit = m_voxel.begin(); cit != m_voxel.end(); ++cit){
		samplePt.push_back((this->GetRoadmap()->GetGraph()->GetVertex(*cit)).GetPoint());
	}


}


template<class MPTraits>
void
VRIncremental<MPTraits>::
Finalize() {
	// Output final map.
	this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());       

	// Output stats.
	ofstream  osStat(this->GetBaseFilename() + ".stat");
	StatClass* stats = this->GetStatClass();
	stats->PrintAllStats(osStat, this->GetRoadmap());   
}

#endif
