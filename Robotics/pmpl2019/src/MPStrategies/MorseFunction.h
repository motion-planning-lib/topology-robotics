#ifndef MORSE_FUNCTION_H_
#define MORSE_FUNCTION_H_

#include <map>
#include <vector>

#include "VRhomotopy.h"
#include "Environment/Boundary.h"
#include "Environment/BoundingSphere.h"
#include "Environment/Environment.h"
#include "Environment/Body.h"
#include "Environment/FixedBody.h"
#include "containers/sequential/graph/algorithms/graph_algo_util.h"

////////////////////////////////////////////////////////////////////////////////////////
// Calculates critical points for c-obstacle.
// Adds feasible critical point to the roadmap.
////////////////////////////////////////////////////////////////////////////////////////

template<class MPTraits>
class MorseFunction : public VRhomotopy<MPTraits> {

	public:

		typedef typename MPTraits::CfgType CfgType;
		typedef typename MPTraits::MPProblemType MPProblemType;
		typedef typename MPProblemType::RoadmapType RoadmapType;
		typedef typename RoadmapType::GraphType GraphType;
		typedef typename MPProblemType::SamplerPointer SamplerPointer;
		typedef typename GraphType::vertex_descriptor VID;
		typedef typename MPProblemType::NeighborhoodFinderPointer NeighborhoodFinderPointer;
		typedef typename MPProblemType::MapEvaluatorPointer MapEvaluatorPointer;
		typedef typename GraphType::ColorMap ColorMap;
		typedef typename ColorMap::property_value_type color_value;

		MorseFunction(const vector<string>& _evaluatorLabels = vector<string>());

		MorseFunction(typename MPTraits::MPProblemType* _problem, XMLNode& _node);

		virtual ~MorseFunction() {}

		// virtual void ParseXML(XMLNode& _node);
		virtual void Print(ostream& _os) const;

		virtual void Initialize();
		virtual void Run();
		virtual void Finalize();

	protected:

		void AddCriticalPts(map<Point3d, double>& morsePt);
		void GetSampleNeighbors(map<Point3d, vector<VID>>& samplePt, map<Point3d, double>& morsePt);


};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//******************Construction**********************
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template<class MPTraits>
MorseFunction<MPTraits>::
MorseFunction(const vector<string>& _evaluatorLabels): VRhomotopy<MPTraits>(_evaluatorLabels){
	this->SetName("MorseFunction");
}

template<class MPTraits>
MorseFunction<MPTraits>::
MorseFunction(typename MPTraits::MPProblemType* _problem, XMLNode& _node) :
	VRhomotopy<MPTraits>(_problem, _node) {
		this->SetName("MorseFunction");
	}


template<class MPTraits>
void
MorseFunction<MPTraits>::
Print(ostream& _os) const {
	VRhomotopy<MPTraits>::Print(_os);
}

template<class MPTraits>
void MorseFunction<MPTraits>::Initialize(){
	VRhomotopy<MPTraits>::Initialize();
}

template<class MPTraits>
void MorseFunction<MPTraits>::Run(){
	VRhomotopy<MPTraits>::Run();

	GraphType* g = new GraphType();
	this->GetRoadmap()->SetGraph(g);
	StatClass* stats = this->GetStatClass();

	//vector<VID> sample = VRhomotopy<MPTraits>::GetNeighborPts();

	string tick = "Distance and density Computation time";
	stats->StartClock(tick);
	map<Point3d, double> dist = VRhomotopy<MPTraits>::GetDistanceSet();
	map<Point3d, int> dense = VRhomotopy<MPTraits>::GetDensitySet();
	set<Point3d> boundaryPt = VRhomotopy<MPTraits>::GetObstaclePt();
	stats->StopClock(tick);

	map<Point3d, double> morse;
	//multiset<double> store;
	//vector<Vector3d> vertexList;

	string t = "Total Calculation time";
	stats->StartClock(t);

	for(typename set<Point3d>::iterator bit = boundaryPt.begin(); bit != boundaryPt.end(); ++bit){
		double m = dist[*bit] * dense[*bit];
		//cout<<"Morse Values:"<<m<<endl;
		//store.insert(m);
		morse[*bit] = m;
	}
	AddCriticalPts(morse);
	//store.clear();
	stats->StopClock(t);

	map<Point3d, vector<VID>> sample = VRhomotopy<MPTraits>::GetNeighborPts();
	GetSampleNeighbors(sample, morse);

}

template<class MPTraits>
void
MorseFunction<MPTraits>::
Finalize() {
	//Output final map
	this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());
	this->GetBlockRoadmap()->Write(this->GetBaseFilename() + ".block.map", this->GetEnvironment());

	// Output stats.
	ofstream  osStat(this->GetBaseFilename() + ".stat");
	StatClass* stats = this->GetStatClass();
	stats->PrintAllStats(osStat, this->GetRoadmap());

}

template<class MPTraits> 
void 
MorseFunction<MPTraits>::
AddCriticalPts(map<Point3d, double>& morsePt){
	vector<CfgType> critPt;

	for(typename map<Point3d, double>::iterator mit = morsePt.begin(); mit != morsePt.end(); ++mit){
		if(mit->second != 0) {
			CfgType cfg(mit->first, size_t(-1));
			critPt.push_back(cfg);
		}
	}

	GraphType* gp = this->GetMPProblem()->GetBlockRoadmap()->GetGraph();
	ColorMap color_map;

	for(typename vector<CfgType>::iterator Cp = critPt.begin(); Cp != critPt.end(); ++Cp){
		VID node = gp->AddVertex(*Cp);
		color_map.put(node, 1);
	}

}

template<class MPTraits> 
void 
MorseFunction<MPTraits>::
GetSampleNeighbors(map<Point3d, vector<VID>>& samplePt, map<Point3d, double>& morsePt){
	cout<<"Inside sample neighbors"<<endl;
	GraphType* graph = this->GetRoadmap()->GetGraph();
	GraphType* prev_g = VRhomotopy<MPTraits>::GetParentGraph();

	for(typename map<Point3d, double>::iterator mit = morsePt.begin(); mit != morsePt.end(); ++mit){
		if(mit->second != 0){
			vector<VID> fcPt(samplePt[mit->first]);
			for(typename vector<VID>::iterator sit = fcPt.begin(); sit != fcPt.end(); ++sit){
				//CfgType cfg_node = prev_g->GetVertex(sit);

				VID sample_node = graph->AddVertex(prev_g->GetVertex(sit));
				if(this->m_debug)
					cout<<"Inserting node:"<<sample_node<<endl;
			}
		}
	}
}



#endif
