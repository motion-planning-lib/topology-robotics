#ifndef VRHOMOTOPY_H_
#define VRHOMOTOPY_H_

#include "Environment/Boundary.h"
#include "Environment/BoundingSphere.h"
#include "Environment/Environment.h"
#include "Environment/Body.h"
#include "Environment/FixedBody.h"
#include "MPStrategyMethod.h"
#include "Utilities/MedialAxisUtilities.h"

#define PI 3.14159265

template<class MPTraits>
class VRhomotopy : public MPStrategyMethod<MPTraits> {
	public:
		typedef typename MPTraits::CfgType CfgType;
		typedef typename MPTraits::MPProblemType MPProblemType;
		typedef typename MPProblemType::RoadmapType RoadmapType;
		typedef typename RoadmapType::GraphType GraphType;
		typedef typename MPProblemType::SamplerPointer SamplerPointer;
		typedef typename GraphType::vertex_descriptor VID;
		typedef typename MPProblemType::NeighborhoodFinderPointer NeighborhoodFinderPointer;
		typedef typename MPProblemType::MapEvaluatorPointer MapEvaluatorPointer;


		VRhomotopy(const vector<string>& _evaluatorLabels = vector<string>());

		VRhomotopy(typename MPTraits::MPProblemType* _problem, XMLNode& _node);

		virtual ~VRhomotopy() {}

		virtual void ParseXML(XMLNode& _node);
		virtual void Print(ostream& _os) const;

		virtual void Initialize();
		virtual void Run();
		virtual void Finalize();

		double GetHausdroffDistance(vector<Point3d>& hullPoints, set<Point3d>& boundaryPoints);
		void ConvexHull(vector<Point3d>& samplePoints, vector<Point3d>& hullPoints, Point3d p, Point3d q, int dir);
		int orientation(Point3d& p, Point3d& q, Point3d& r);
		int lineDist(Point3d& p, Point3d& q, Point3d& r);
		map<Point3d, double> MinDisttoObstacle(vector<Point3d>& SamplePoints, set<Point3d>& boundaryPoints);
		map<Point3d, int> DensityMethod(set<Point3d>& boundaryPoints/*, vector<VID>& collisionPts*/);

		//access parent graph object
		GraphType* GetParentGraph() { return graph;}

		//access data members
		set<Point3d> GetObstaclePt();
		map<Point3d, double> GetDistanceSet();
		map<Point3d, int> GetDensitySet();
		map<Point3d, vector<VID>> GetNeighborPts();

	protected:
		string m_samplerLabel;
		GraphType* graph;
		map<Point3d, vector<VID>> neighborPt;
		//map<VID, Point3d> samples;
		vector<Point3d> samplePt;
		set<Point3d> ObstaclePts;

};

template<class MPTraits>
VRhomotopy<MPTraits>::
VRhomotopy(const vector<string>& _evaluatorLabels){
	this->m_meLabels = _evaluatorLabels;
	this->SetName("VRhomotopy");
}

template<class MPTraits>
VRhomotopy<MPTraits>::
VRhomotopy(typename MPTraits::MPProblemType* _problem, XMLNode& _node) :
	MPStrategyMethod<MPTraits>(_problem, _node) {
		this->SetName("VRhomotopy");
		ParseXML(_node);
	}

template<class MPTraits>
void
VRhomotopy<MPTraits>::
ParseXML(XMLNode& _node) {
	for(auto& child : _node) {
		if(child.Name() == "Sampler") {
			m_samplerLabel = child.Read("method", true, "", "Node Generation Method");
		}
		if(child.Name() == "Evaluator") {
			this->m_meLabels.push_back(child.Read("method", true, "", "Evaluation Method"));
		}
	}
}

template<class MPTraits>
void
VRhomotopy<MPTraits>::
Print(ostream& _os) const {
	MPStrategyMethod<MPTraits>::Print(_os);
	_os << "\tSampling method: " << m_samplerLabel << endl;

	_os<<"\tMapEvaluator :";
	for(const auto& label : this->m_meLabels)
		_os << "\t" << label << endl;
}

template<class MPTraits>
void VRhomotopy<MPTraits>::Initialize(){
	Print(cout);

	for(const auto& label: this->m_meLabels) {
		MapEvaluatorPointer evaluator = this->GetMapEvaluator(label);
		if(evaluator->HasState())
			evaluator->operator()();
	}

}


template<class MPTraits>
void VRhomotopy<MPTraits>::Run(){

	bool mapPassedEvaluation = this->EvaluateMap();
	RoadmapType* rmap = this->GetMPProblem()->GetRoadmap();
	shared_ptr<Boundary> boundary;

        auto robType = this->GetEnvironment()->GetRobot(0)->GetBaseType();
	if(robType == FreeBody::BodyType::Fixed){
		double robot_rad = this->GetEnvironment()->GetRobot(0)->GetBoundingSphereRadius();
		//cout<<this->GetEnvironment()->GetRobot(0)->GetCenterOfMass()<<endl;
		shared_ptr<Boundary> b(new BoundingSphere(this->GetEnvironment()->GetRobot(0)->GetCenterOfMass(), robot_rad));
		this->GetEnvironment()->SetBoundary(b);
	}
	boundary = this->GetEnvironment()->GetBoundary();
	//cout<<boundary->GetCenter()<<endl;
	
	set<Point3d> boundaryPt;
	//vector<Point3d> samples;
	vector<Point3d> hullPt;
	vector<CfgType> vectorCfgs;
	//vector<Point3d> samplePt;

	StatClass* stats = this->GetStatClass();
	string clockName = "Total Node Generation";

	while(!mapPassedEvaluation){

		SamplerPointer sampler = this->GetMPProblem()->GetSampler(m_samplerLabel);
		stats->StartClock(clockName);
		sampler->Sample(100, 1, this->m_boundary, back_inserter(vectorCfgs));
		stats->StopClock(clockName);

		for(typename vector<CfgType>::iterator C = vectorCfgs.begin(); C != vectorCfgs.end(); ++C) {
			//CfgType& n = this->GetRoadmap()->GetGraph()->GetVertex(VID);
			if(C->IsLabel("VALID") && C->GetLabel("VALID")) {
				stats->StartClock(clockName);
				VID newVID = rmap->GetGraph()->AddVertex(*C);
				//samplePt.push_back(newVID);
				//cout<<"New vertex added:"<< newVID <<endl;
				stats->StopClock(clockName);

				if(this->m_debug) {
					cout<<"Node vertex ID "<< newVID <<endl;
				}
				Vector3d _s, b;

                                if(robType == FreeBody::BodyType::Fixed){
					//(*C).ResetRigidBodyCoordinates();
					(*C).ConfigureRobot();
					shared_ptr<vector<Vector3d> > joints = shared_ptr<vector<Vector3d> >(new vector<Vector3d>);
					this->GetEnvironment()->GetRobot((*C).GetRobotIndex())->PolygonalApproximation(*joints);
					_s = (*joints)[joints->size()-1];
					//cout<<"joints = "<<joints->size()<<endl;
					b = boundary->GetClearancePoint(_s);
				}
				else {
					_s = (*C).GetPoint();
					b = boundary->GetClearancePoint(_s);
				}
				samplePt.push_back(_s);
				//samples[newVID] = _s;
				boundaryPt.insert(b);
				//samplePt.push_back((*C).GetPoint());
			}
		}

		mapPassedEvaluation = this->EvaluateMap();
	}
	
	Point3d min_p, max_p;
	if(samplePt.size() > 3){
		min_p = samplePt.front();
		max_p = samplePt.back();
		for (typename vector<Point3d>::iterator pt = samplePt.begin(); pt != samplePt.end(); ++pt){
			if(*pt[0] < min_p[0])
				min_p = *pt;

			else if(*pt[0] > max_p[0])
				max_p = *pt;
	        }
	}
        
	ConvexHull(samplePt, hullPt, min_p, max_p, 1); //clockwise
	ConvexHull(samplePt, hullPt, min_p, max_p, 2); //counterclockwise
         
	ObstaclePts = GetObstaclePt();

	string tick = "Total Computation time";
	stats->StartClock(tick);

	double epsilon = GetHausdroffDistance(hullPt, boundaryPt);
	cout<<"Hausdroff Distance: "<< epsilon <<endl;

	stats->StopClock(tick);

	int n = (vectorCfgs.front()).DOF();
	double mu = sqrt(2*n/(n+1));
	double alpha = 2*epsilon/(2-mu);
	cout<<"alpha :"<<alpha<<endl;
	//cout<<"DOF of robot is "<<n<<endl;
	
	//current graph for child class
	graph = rmap->GetGraph();
}

template<class MPTraits>
int VRhomotopy<MPTraits>::
orientation(Point3d& p, Point3d& q, Point3d& r) {
	int val = (q[1] - p[1]) * (r[0] - q[0]) -
		(q[0] - p[0]) * (r[1] - q[1]);

	if (val == 0) return 0;  // collinear
	return (val > 0)? 1: 2; //clockwise or counterclockwise
}

template<class MPTraits>
int VRhomotopy<MPTraits>::
lineDist(Point3d& p, Point3d& q, Point3d& r) {
        return abs ((q[1] - p[1]) * (r[0] - q[0]) -
                (q[0] - p[0]) * (r[1] - q[1]));
}

template<class MPTraits>
void VRhomotopy<MPTraits>::
ConvexHull(vector<Point3d>& samplePoints, vector<Point3d>& hullPoints, Point3d p, Point3d q, int dir) {
	Point3d ind;
	int max_dist = 0;

	for (typename vector<Point3d>::iterator S = samplePoints.begin(); S != samplePoints.end(); ++S){
		int temp = lineDist(p, q, *S);
		if(orientation(p, q, *S) == dir && temp > max_dist)
		{
			ind = *S;
			max_dist = temp;
		}
	}

	if(find(samplePoints.begin(), samplePoints.end(), ind) == samplePoints.end()){
		hullPoints.push_back(p);
		hullPoints.push_back(q);
		return;
	}

	ConvexHull(samplePoints, hullPoints, ind, p, -orientation(ind, p, q));
	ConvexHull(samplePoints, hullPoints, ind, q, -orientation(ind, q, p));
/*	
	Point3d l = samplePoints.front();
	for (typename vector<Point3d>::iterator S = samplePoints.begin(); S != samplePoints.end(); ++S){
		if (*S[0] < l[0])
			l = *S;
	}

	Point3d p = l, q;
	do
	{
		// Add current point to result
		hullPoints.push_back(p);

		int size = samplePoints.size();
		q = samplePoints.at(rand()%size);
		for (typename vector<Point3d>::iterator sit = samplePoints.begin(); sit != samplePoints.end(); ++sit)
		{
			// update q
			if (orientation(p, *sit, q) == 2)
				q = *sit;
		}

		// Now q is the most counterclockwise with respect to p
		// Set p as q for next iteration, so that q is added to
		// result 'hull'
		p = q;

	} while (p != l);  // While we don't come to first point
	*/

} 

template<class MPTraits>
double VRhomotopy<MPTraits>::
GetHausdroffDistance(vector<Point3d>& hullPoints, set<Point3d>& boundaryPoints) {
	double h = 0.0;
	double m = 0.0;

	for(typename vector<Point3d>::iterator hit = hullPoints.begin(); hit != hullPoints.end(); ++hit) {
		double min = INFINITY;
		for(typename set<Point3d>::iterator bit = boundaryPoints.begin(); bit != boundaryPoints.end(); ++bit) { 
			const Vector3d diff = (*hit) - (*bit);
			double d1 = diff.norm();

			if(min > d1)
				min = d1;    
		}
		if(h < min)
			h = min; 
	}
        //cout<<"h= "<<h<<endl;
	
	for(typename set<Point3d>::iterator kit = boundaryPoints.begin(); kit != boundaryPoints.end(); ++kit) {
		double minima = INFINITY;
		for(typename vector<Point3d>::iterator dit = hullPoints.begin(); dit != hullPoints.end(); ++dit) {
			const Vector3d sub = (*kit) - (*dit);
			double d2 = sub.norm();
			if(minima > d2)
				minima = d2; 
		}
		if(m < minima)
			m = minima;
	}
        //cout<<"m= "<<m<<endl;
	
	if(m > h)
		return m;
	else
	        return h;
}

template<class MPTraits>
void
VRhomotopy<MPTraits>::
Finalize() {
	// Output final map.
	this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());       

	// Output stats.
	ofstream  osStat(this->GetBaseFilename() + ".stat");
	StatClass* stats = this->GetStatClass();
	stats->PrintAllStats(osStat, this->GetRoadmap());   
}

template<class MPTraits>
map<Point3d, double>
VRhomotopy<MPTraits>::
MinDisttoObstacle(vector<Point3d>& SamplePoints, set<Point3d>& boundaryPoints){
	map<Point3d, double> mindist;
	
	for(typename set<Point3d>::iterator git = boundaryPoints.begin(); git != boundaryPoints.end(); ++git) {
		double minima = INFINITY;
		Point3d v3;
		for(typename vector<Point3d>::iterator it = SamplePoints.begin(); it != SamplePoints.end(); ++it) {
			const Vector3d sub = (*git) - (*it);
			double d3 = sub.norm();
                                              
			if(minima > d3){
				minima = d3;
				v3 = *it;
			}
		}
		
		mindist.insert({*git, minima});
	}
	CfgType closest(v3, size_t(-1));
	cout<<"ClosestNode\t"<<closest<<endl;

	return mindist;

}

template<class MPTraits>
map<Point3d, int>
VRhomotopy<MPTraits>::
DensityMethod(set<Point3d>& boundaryPoints /*,vector<VID>& collisionPts*/){
	auto nf = this->GetNeighborhoodFinder("RadiusNF");
	map<Point3d, int> density;
	//cout<< nf->GetRadius()<<endl;
	
	//this is just to identify pocket for biomolecule.
	/*map<Point3d, double> dist = GetDistanceSet();
	auto pocket = min_element(dist.begin(), dist.end(), 
			[](const pair<Point3d,double>& x, const pair<Point3d,double>& y)->bool{
                        return x.second < y.second; });
        cout<<"The nodes around cavity are:"<<endl;*/

	for(typename set<Point3d>::iterator ait = boundaryPoints.begin(); ait != boundaryPoints.end(); ++ait) {
		CfgType cfg(*ait, size_t(-1));
		int d = 0;

		vector<pair<VID, double>> neighbors;
		
		nf->FindNeighbors(this->GetMPProblem()->GetRoadmap(), graph->begin(), 
				graph->end(), false, cfg, back_inserter(neighbors));

		if(neighbors.empty())
			continue;

		for(const auto& nb : neighbors){
			neighborPt[*ait].push_back(nb.first);

			/*if(pocket->first == *ait){
				double d_min = ((*ait) - graph->GetVertex(nb.first).GetPoint()).norm();
			        if(pocket->second == d_min)
					cout<<graph->GetVertex(nb.first)<<endl;
			}*/
		}
		d = (neighborPt[*ait]).size();

		cout<<*ait<<"density:"<<d<<endl;
		density.insert({*ait, d});        

	}

	return density;
}

template<class MPTraits>
map<Point3d, double> 
VRhomotopy<MPTraits>::GetDistanceSet(){
	return MinDisttoObstacle(samplePt, ObstaclePts);
}

template<class MPTraits>
map<Point3d, int>
VRhomotopy<MPTraits>::GetDensitySet(){
	return DensityMethod(ObstaclePts /*, samplePt*/);
}

template<class MPTraits>
map<Point3d, vector<typename VRhomotopy<MPTraits>::VID>> 
VRhomotopy<MPTraits>::GetNeighborPts(){
	return neighborPt;
}

template<class MPTraits>
set<Point3d>
VRhomotopy<MPTraits>::GetObstaclePt(){

	for(size_t i = 0; i < this->GetEnvironment()->NumObstacles(); ++i) {
		GMSPolyhedron& polyhedron = this->GetEnvironment()->GetObstacle(i)->GetFixedBody(0)->GetWorldPolyhedron();
		vector<Vector3d> vertexList = polyhedron.GetVertexList();

		for(typename vector<Vector3d>::iterator lit = vertexList.begin(); lit != vertexList.end(); ++lit) {
			ObstaclePts.insert(*lit);
		}

	}
	return ObstaclePts;						                                         
}


#endif
