#ifndef RRG_H_
#define RRG_H_

#include "BasicRRTStrategy.h"
//#include "Connector.h"
//#include "MPProblem.h"
//#include "MPStrategy.h"
#include "Environment/Environment.h"

template<class MPTraits>
class RRG : public BasicRRTStrategy<MPTraits> {
  public:
    typedef typename MPTraits::CfgType          CfgType;
    typedef typename MPTraits::CfgRef           CfgRef;
    typedef typename MPTraits::WeightType       WeightType;
    typedef typename MPTraits::MPProblemType    MPProblemType;
    typedef typename MPProblemType::RoadmapType RoadmapType;
    typedef typename MPProblemType::GraphType   GraphType;
    typedef typename MPProblemType::VID         VID;
    typedef typename MPProblemType::ConnectorPointer ConnectorPointer;

     RRG() {
     this->SetName("RRG");
     }

    RRG(MPProblemType* _problem, XMLNode& _node);
    virtual ~RRG() {}
    
    virtual void Initialize() override;
    virtual void ParseXML(XMLNode& _node);
    virtual void Print(ostream& _os) const;
    virtual VID ExpandTree(CfgType& _dir);

  private:
    string m_nc;  // Contains the Node Connection Method string
};

template<class MPTraits>
void
RRG<MPTraits>::
Initialize() {
  BasicRRTStrategy<MPTraits>::Initialize();
 // for(auto v  : *this->GetRoadmap()->GetGraph())
   // v.property().SetStat(RLabel(), numeric_limits<double>::max());
}

template<class MPTraits>
RRG<MPTraits>::
RRG(MPProblemType* _problem, XMLNode& _node) :
  BasicRRTStrategy<MPTraits>(_problem, _node){
    ParseXML(_node);
   // _node.warnUnrequestedAttributes();
}

template<class MPTraits>
void
RRG<MPTraits>::
ParseXML(XMLNode& _node) {
  m_nc = _node.Read("connectionMethod",true,"","Node Connection Method");
  //if(m_debug) 
    // Print(cout);
}

template<class MPTraits>
void
RRG<MPTraits>::Print(ostream& _os) const {
  BasicRRTStrategy<MPTraits>::Print(_os);
  _os << "\tNode Connection:: " << m_nc << endl;
}

template<class MPTraits>
typename RRG<MPTraits>::VID
RRG<MPTraits>::
ExpandTree(CfgType& _dir) {

  VID vid = BasicRRTStrategy<MPTraits>::ExpandTree(_dir);

  // After expanding, attempt connections to recent node
  if (vid != INVALID_VID) {
   // vector<VID> allVIDs;
    vector<VID> currentVID;
    currentVID.push_back(vid);
    vector<CfgType> col;

    //CfgType& allvids = this->GetMPProblem()->GetRoadmap()->GetGraph()->GetVertex(allVIDs);
    
    ConnectorPointer pConnection = this->GetConnector(m_nc);
   
    // Calling Connect Method and connecting nodes
    stapl::sequential::vector_property_map<typename GraphType::GRAPH,size_t > cmap;
    
    RoadmapType* rm = this->GetMPProblem()->GetRoadmap();
    pConnection->Connect(rm, currentVID.begin(), currentVID.end(), back_inserter(col));
    
  }
  return vid;
}

#endif
