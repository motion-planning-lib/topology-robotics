#ifndef PRIORITY_RANKER_H_
#define PRIORITY_RANKER_H_

#include "MPStrategyMethod.h"
#include "MapEvaluators/PathQuery.h"

// This method ranks the topological properties of C-Space to prioritize the importance of diverse paths.

template<class MPTraits>
class PriorityRanker : public MPStrategyMethod<MPTraits> {
	public:

		typedef typename MPTraits::CfgType CfgType;
		typedef typename MPTraits::MPProblemType MPProblemType;
		typedef typename MPProblemType::RoadmapType RoadmapType;
		typedef typename MPProblemType::GraphType GraphType;
		typedef typename MPProblemType::VID VID;
		typedef typename MPProblemType::ConnectorPointer ConnectorPointer;
		typedef typename MPProblemType::MapEvaluatorPointer MapEvaluatorPointer;

		PriorityRanker(string _inputMapFilename = "", string _inputPathFilename1 = "",
				string _inputPathFilename2 = "", string _inputPathFilename3 = "", 
				string _connectorLabels = "kClosest", vector<string> _evaluatorLabels = vector<string>());

		PriorityRanker(typename MPTraits::MPProblemType* _problem, XMLNode& _node);
		virtual ~PriorityRanker() {}

		virtual void ParseXML(XMLNode& _node);
		virtual void Print(ostream& _os) const;

		virtual void Initialize();
		virtual void Run();
		virtual void Finalize();

		void FindImportance(map<int, vector<VID>>& degree, priority_queue<pair<double, pair<VID, VID>>>& edgeW,
				queue<pair<int,double>>& cost);
		
		void RankPaths(queue<pair<int,double>>& cost, pair<int, int>& p1, pair<int, int>& p2, pair<int, int>& p3);

	protected:
		string m_inputMapFilename; ///< Input roadmap mapfile
		string m_inputPathFile1; ///< Input roadmap path 
		string m_inputPathFile2; ///< Input roadmap path
		string m_inputPathFile3; ///< Input roadmap path
		GraphType* g1;
		GraphType* g2;
		GraphType* g3;
		string m_connectorLabels;
		vector<pair<string,tuple<int, int, double> > > ranks; ///< stores the ranked paths
};

template<class MPTraits>
PriorityRanker<MPTraits>::
PriorityRanker(string _inputMapFilename, string _inputPathFilename1, 
		string _inputPathFilename2, string _inputPathFilename3,
		string _connectorLabels, vector<string> _evaluatorLabels) :
	m_inputMapFilename(_inputMapFilename), m_inputPathFile1(_inputPathFilename1), 
	m_inputPathFile2(_inputPathFilename2), m_inputPathFile3(_inputPathFilename3), m_connectorLabels(_connectorLabels){
		this->m_meLabels = _evaluatorLabels;
		this->SetName("PriorityRanker");
	}


template<class MPTraits>
PriorityRanker<MPTraits>::
PriorityRanker(typename MPTraits::MPProblemType* _problem, XMLNode& _node) :
	MPStrategyMethod<MPTraits>(_problem, _node), m_inputMapFilename(""), 
	m_inputPathFile1(""), m_inputPathFile2(""), m_inputPathFile3("") {
		this->SetName("PriorityRanker");
		ParseXML(_node);
	}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
ParseXML(XMLNode& _node) {
	m_inputMapFilename = _node.Read("inputMap", true, "", "filename of roadmap to start from");
	m_inputPathFile1 = _node.Read("inputPath1", true, "", "filename of the path");
	m_inputPathFile2 = _node.Read("inputPath2", true, "", "filename of the path");
	m_inputPathFile3 = _node.Read("inputPath3", true, "", "filename of the path");

	for(auto& child : _node) {
		if(child.Name() == "Connector")
			m_connectorLabels = child.Read("method", false, "", "Connector Label");
		else if(child.Name() == "Evaluator")
			this->m_meLabels.push_back(child.Read("method", false, "", "Evaluator Label"));
	}
}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
Print(ostream& _os) const {
	MPStrategyMethod<MPTraits>::Print(_os);
	_os << "\tInput Map: " << m_inputMapFilename << endl;
	_os << "\tPath 1: " << m_inputPathFile1 << endl;
	_os << "\tPath 2: " << m_inputPathFile2 << endl;
	_os << "\tPath 3: " << m_inputPathFile3 << endl;

	_os << "\tConnectors:: " << m_connectorLabels << endl;

	_os<<"\tMapEvaluators" << endl;
	for(const auto& label : this->m_meLabels)
		_os << "\t\t" << label << endl;


}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
Initialize() {
	if(this->m_debug)
		cout << "Initializing " << this->GetNameAndLabel() << "..." << endl;

	//read in and reload roadmap and evaluators
	if(!m_inputMapFilename.empty()){
		RoadmapType* r = this->GetRoadmap();
		if(this->m_debug)
			cout << "\tLoading roadmap from \"" << m_inputMapFilename << "\".";

		r->Read(m_inputMapFilename.c_str());

		GraphType* g = r->GetGraph();
		for(typename GraphType::VI vi = g->begin(); vi != g->end(); ++vi)
			VDAddNode(g->GetVertex(vi));
		if(this->m_debug) {
			cout << "\tRoadmap has " << g->get_num_vertices() << " nodes and "
				<< g->get_num_edges() << " edges." << endl;
			cout << "\tResetting map evaluator states." << endl;
		}

		if(!m_inputPathFile1.empty()) {
			RoadmapType* r1 = new RoadmapType();
			if(this->m_debug)
				cout << "\tLoading roadmap from \"" << m_inputPathFile1 << "\".";

			r1->Read(m_inputPathFile1.c_str());
			g1 = r1->GetGraph();
			for(typename GraphType::VI vi = g1->begin(); vi != g1->end(); ++vi)
				VDAddNode(g1->GetVertex(vi));
			if(this->m_debug) {
				cout << "\tRoadmap has " << g1->get_num_vertices() << " nodes and "
					<< g1->get_num_edges() << " edges." << endl;
				cout << "\tResetting map evaluator states." << endl;
			}
		}
		if(!m_inputPathFile2.empty()) {
			RoadmapType* r2 = new RoadmapType();
			if(this->m_debug)
				cout << "\tLoading roadmap from \"" << m_inputPathFile2 << "\".";

			r2->Read(m_inputPathFile2.c_str());
			g2 = r2->GetGraph();
			for(typename GraphType::VI vi = g2->begin(); vi != g2->end(); ++vi)
				VDAddNode(g2->GetVertex(vi));
			if(this->m_debug) {
				cout << "\tRoadmap has " << g2->get_num_vertices() << " nodes and "
					<< g2->get_num_edges() << " edges." << endl;
				cout << "\tResetting map evaluator states." << endl;
			}
		}
		if(!m_inputPathFile3.empty()) {
			RoadmapType* r3 = new RoadmapType();
			if(this->m_debug)
				cout << "\tLoading roadmap from \"" << m_inputPathFile3 << "\".";

			r3->Read(m_inputPathFile3.c_str());
			g3 = r3->GetGraph();
			for(typename GraphType::VI vi = g3->begin(); vi != g3->end(); ++vi)
				VDAddNode(g3->GetVertex(vi));
			if(this->m_debug) {
				cout << "\tRoadmap has " << g3->get_num_vertices() << " nodes and "
					<< g3->get_num_edges() << " edges." << endl;
				cout << "\tResetting map evaluator states." << endl;
			}
		}

		for(const auto& label: this->m_meLabels) {
			MapEvaluatorPointer evaluator = this->GetMapEvaluator(label);
			if(evaluator->HasState())
				evaluator->operator()();
		}

	}
}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
Run() {

	GraphType* g = this->GetRoadmap()->GetGraph();
	auto dm = this->GetDistanceMetric("euclidean");
	VID node;
	map<int, vector<VID>> degree;

	StatClass* stats = this->GetStatClass();
	string clock = "Ranking Time";
	stats->StartClock(clock);

	//ranking priority of vertices based on degree of centrality
	for(typename GraphType::VI vi = g->begin(); vi != g->end(); ++vi){
		node = g->GetVID(vi);
		typename GraphType::vertex_iterator vit = g->find_vertex(node);
		int i = 0;
		for(const auto& e : *vit){
			i++;
			if(this->m_debug)
				cout<<"neighbor: "<< e.target() <<endl;
		}

		if(i > 1)
			degree[i].push_back(g->GetVID(vi));
	}

	//ranking edges based on connection length
	priority_queue<pair<double, pair<VID, VID> > > edgeW;
	typename GraphType::vertex_iterator vi;
	typename GraphType::adj_edge_iterator ei;
	for(typename GraphType::VI eit = g->begin(); eit != g->end(); ++eit){
		node = g->GetVID(eit);
		vi = g->find_vertex(node);
		ei = (*vi).begin();
		priority_queue<pair<double, pair<VID, VID> > > line = edgeW;

		while(ei != (*vi).end()){
			vector<pair<VID,VID>> pairs;

			while(!line.empty()){
				pair<double, pair<VID, VID>> top = line.top();
				pairs.push_back(top.second);
				line.pop();
			}
			pair<VID, VID> edge = make_pair((*ei).target(), (*ei).source());

			if(find(pairs.begin(), pairs.end(), edge) != pairs.end()) continue;
			else {
					pair<double, pair<VID,VID>> single_edge;
					single_edge.first = (*ei).property().GetWeight();
					single_edge.second.first = (*ei).source();
					single_edge.second.second = (*ei).target();
					edgeW.push(single_edge);
			}
			ei++;
		}
	} 

	//ranking paths based on cost
	double l1, l2, l3;
        set<double> lengths;

        //first path
        for(typename GraphType::VI pi = g1->begin(); next(pi) != g1->end(); ++pi){
                auto npi = next(pi,1);
                if(g->IsVertex(g1->GetVertex(pi)) && g->IsVertex(g1->GetVertex(npi))){
                        if(g->GetVID(g1->GetVertex(pi)) == g->GetVID(g1->GetVertex(npi)))
                                continue;
                        typename GraphType::edge_descriptor ed(g->GetVID(g1->GetVertex(pi)), g->GetVID(g1->GetVertex(npi)));
                        if(g->find_edge(ed, vi, ei))
                                l1 += (*ei).property().GetWeight();
                }
        }
        if(l1 != 0)
                lengths.insert(l1);

	//second path
        for(typename GraphType::VI qi = g2->begin(); next(qi) != g2->end(); ++qi){
                auto nqi = next(qi, 1);
                if(g->IsVertex(g2->GetVertex(qi)) && g->IsVertex(g2->GetVertex(nqi))){
                        if(g->GetVID(g2->GetVertex(qi)) == g->GetVID(g2->GetVertex(nqi)))
                                continue;
                        typename GraphType::edge_descriptor ed(g->GetVID(g2->GetVertex(qi)), g->GetVID(g2->GetVertex(nqi)));
                        if(g->find_edge(ed, vi, ei))
                                l2 += (*ei).property().GetWeight();
                }

        }
        if(l2 != 0)
                lengths.insert(l2);

        //third path
        for(typename GraphType::VI ri = g3->begin(); next(ri) != g3->end(); ++ri){
                auto nri = next(ri, 1);
                if(g->IsVertex(g3->GetVertex(ri)) && g->IsVertex(g3->GetVertex(nri))){
                        if(g->GetVID(g3->GetVertex(ri)) == g->GetVID(g3->GetVertex(nri)))
                                continue;
                        typename GraphType::edge_descriptor ed(g->GetVID(g3->GetVertex(ri)), g->GetVID(g3->GetVertex(nri)));
                        if(g->find_edge(ed, vi, ei))
                                l3 += (*ei).property().GetWeight();
                }

        }
	if(l3 != 0)
                lengths.insert(l3);


//	double l1 = ceil(g1->get_num_vertices() * 0.92), l2 = ceil(g2->get_num_vertices() * 0.92), 
//	       l3 = ceil(g3->get_num_vertices() * 0.92);
//	set<double> lengths;
//	
//	if(l1 != 0)
//		lengths.insert(l1);
//	if(l2 != 0)
//		lengths.insert(l2);
//	if(l3 != 0)
//		lengths.insert(l3);

	queue<pair<int,double>> cost;
	cout<<"No. of paths are "<< lengths.size()<<endl;
	for(typename set<double>::iterator lit = lengths.begin(); lit != lengths.end(); ++lit){
		if(*lit == l1)
			cost.push(make_pair(1, l1));
		else if(*lit == l2)
			cost.push(make_pair(2, l2));
		else if(*lit == l3)
			cost.push(make_pair(3, l3));
	}

	stats->StopClock(clock);

	//Get the priority
	FindImportance(degree, edgeW, cost);

	//Display rankings
	cout<<"The priorities of the paths are: "<<endl;
	int i = 1;
	for(typename vector<pair<string,tuple<int, int, double>>>::iterator it = ranks.begin(); it != ranks.end(); ++it){
		pair<string,tuple<int, int, double>> rk = *it;
		cout<<rk.first<<" ranked "<<i<<endl;
		
		tuple<int, int, double> values = rk.second;
		cout<<"Priority Nodes: "<<get<0>(values)<<endl;
		cout<<"Priority Edges: "<<get<1>(values)<<endl;
		cout<<"Path Cost: "<<get<2>(values)<<endl;
		cout<<endl;
		i++;
	}
	cout<<"THE END"<<endl;

}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
FindImportance(map<int, vector<VID>>& degree, priority_queue<pair<double, pair<VID, VID>>>& edgeW, 
		queue<pair<int,double>>& cost) {
	//min(degree) & max(edgeW) & min(cost) ==> higher the value more the importance.
	//nodes has more priority than edges.

	GraphType* g = this->GetRoadmap()->GetGraph();

	//important nodes
	vector<VID> order;
	if(!degree[4].empty() || !degree[5].empty()){
		order = degree[4];
		order.insert(order.end(), degree[5].begin(), degree[5].end());
	}

	//max edge length
	pair<double, pair<VID, VID>> pipe = edgeW.top();
	double dist = pipe.first;
	pair<int,int> p1, p2, p3;

	for(typename GraphType::VI pi = g1->begin(); pi != g1->end(); ++pi){ 
		VID node = g->GetVID(g1->GetVertex(pi));
		if(!order.empty()){
			if(find(order.begin(), order.end(), node) != order.end())
				p1.first++;
		}
	}


	for(typename GraphType::VI qi = g2->begin(); qi != g2->end(); ++qi){
		VID node = g->GetVID(g2->GetVertex(qi));
		if(!order.empty()){
			if(find(order.begin(), order.end(), node) != order.end())
				p2.first++;
		}
	}


	for(typename GraphType::VI ri = g3->begin(); ri != g3->end(); ++ri){
		VID node = g->GetVID(g3->GetVertex(ri));
		if(!order.empty()){
			if(find(order.begin(), order.end(), node) != order.end())
				p3.first++;
		}
	}

	//priority_queue<pair<double, pair<VID, VID> > > edges = edgeW;
	while(!edgeW.empty()){
		pair<double, pair<VID, VID> > top = edgeW.top();
		if(top.first > dist/2){
			if(g1->IsVertex(g->GetVertex(top.second.first)) && g1->IsVertex(g->GetVertex(top.second.second))){
					p1.second++;
			}
			else if(g2->IsVertex(g->GetVertex(top.second.first)) && g2->IsVertex(g->GetVertex(top.second.second))){
                                        p2.second++;
                        }
			else if(g3->IsVertex(g->GetVertex(top.second.first)) && g3->IsVertex(g->GetVertex(top.second.second))){
                                        p3.second++;
                        }
		}
			edgeW.pop();
	}

	RankPaths(cost, p1, p2, p3);
}


template<class MPTraits>
void
PriorityRanker<MPTraits>::
RankPaths(queue<pair<int,double>>& cost, pair<int, int>& p1, pair<int, int>& p2, pair<int, int>& p3) {

	//sorting ranks with path cost
	while(!cost.empty()){

		pair<int, double> l_dist = cost.front();

		if(l_dist.first == 1){
			cost.pop();
			pair<int, double> nxt = cost.front();
			if(l_dist.second == nxt.second){
				if(nxt.first == 2){
					if(p1.first > p2.first)
						ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, l_dist.second)));

					else if(p1.first == p2.first){
						if(p1.second > p2.second)
							ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, l_dist.second)));
						else
							ranks.push_back(make_pair("Path 2",make_tuple(p2.first, p2.second, nxt.second)));
					}

					else
						ranks.push_back(make_pair("Path 2",make_tuple(p2.first, p2.second, nxt.second)));
				}
				else {
					if(p1.first > p3.first)
						ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, l_dist.second)));

					else if(p1.first == p3.first){
						if(p1.second > p3.second)
							ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, l_dist.second)));
						else
							ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, nxt.second)));
					}

					else
						ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, nxt.second)));
				}
			}

			else
				ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, l_dist.second)));
		}

		else if(l_dist.first == 2){
			cost.pop();
			pair<int, double> nxt = cost.front();
			if(l_dist.second == nxt.second){
				if(nxt.first == 3){
					if(p2.first > p3.first)
						ranks.push_back(make_pair("Path 2", make_tuple(p2.first, p2.second, l_dist.second)));

					else if(p2.first == p3.first){
						if(p1.second > p3.second)
							ranks.push_back(make_pair("Path 2", make_tuple(p2.first, p2.second, l_dist.second)));
						else
							ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, nxt.second)));
					}

					else
						ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, nxt.second)));
				}
				else {
                                        if(p1.first > p2.first)
                                                ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, nxt.second)));

                                        else if(p1.first == p2.first){
                                                if(p1.second > p2.second)
                                                        ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, nxt.second)));
                                                else
                                                        ranks.push_back(make_pair("Path 2",make_tuple(p2.first, p2.second, l_dist.second)));
                                        }

                                        else
                                                ranks.push_back(make_pair("Path 2",make_tuple(p2.first, p2.second, l_dist.second)));
                                }
			}

			else
				ranks.push_back(make_pair("Path 2", make_tuple(p2.first, p2.second, l_dist.second)));

		}

		else {
			cost.pop();
			pair<int, double> nxt = cost.front();
                        if(l_dist.second == nxt.second){
                                if(nxt.first == 2){
                                        if(p2.first > p3.first)
                                                ranks.push_back(make_pair("Path 2", make_tuple(p2.first, p2.second, nxt.second)));

                                        else if(p2.first == p3.first){
                                                if(p1.second > p3.second)
                                                        ranks.push_back(make_pair("Path 2", make_tuple(p2.first, p2.second, nxt.second)));
                                                else
                                                        ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, l_dist.second)));
                                        }

                                        else
                                                ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, l_dist.second)));
                                }
				else {
                                        if(p1.first > p3.first)
                                                ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, nxt.second)));

                                        else if(p1.first == p3.first){
                                                if(p1.second > p3.second)
                                                        ranks.push_back(make_pair("Path 1", make_tuple(p1.first, p1.second, nxt.second)));
                                                else
                                                        ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, l_dist.second)));
                                        }

                                        else
                                                ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, l_dist.second)));
                                }
                        }

			else
				ranks.push_back(make_pair("Path 3", make_tuple(p3.first, p3.second, l_dist.second)));
		}
	}
}

template<class MPTraits>
void
PriorityRanker<MPTraits>::
Finalize() {
	// Output final map.
	//this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());

	// Output stats.
	ofstream  osStat(this->GetBaseFilename() + ".stat");
	StatClass* stats = this->GetStatClass();
	stats->PrintAllStats(osStat, this->GetRoadmap());

}

#endif
