#ifndef CFG_MASKING_H_
#define CFG_MASKING_H_

#include "string.h"
#include "MPStrategyMethod.h"
#include "MapEvaluators/PathQuery.h"

// This method masks the configurations of new C-space to avoid path invalidation.

template<class MPTraits>
class CfgMasking : public MPStrategyMethod<MPTraits> {
	public:

		typedef typename MPTraits::CfgType CfgType;
		typedef typename MPTraits::MPProblemType MPProblemType;
		typedef typename MPProblemType::RoadmapType RoadmapType;
		typedef typename MPProblemType::GraphType GraphType;
		typedef typename MPProblemType::VID VID;
		typedef typename MPProblemType::ConnectorPointer ConnectorPointer;
		typedef typename MPProblemType::MapEvaluatorPointer MapEvaluatorPointer;

		CfgMasking(/*string _inputMapFilename = "",*/ string _inputPathFilename1 = "",
				/*string _inputPathFilename2 = "", string _inputPathFilename3 = "",*/ 
				string _connectorLabels = "kClosest", vector<string> _evaluatorLabels = vector<string>());

		CfgMasking(typename MPTraits::MPProblemType* _problem, XMLNode& _node);
		virtual ~CfgMasking() {}

		virtual void ParseXML(XMLNode& _node);
		virtual void Print(ostream& _os) const;

		virtual void Initialize();
		virtual void Run();
		virtual void Finalize();


	protected:
		//string m_inputMapFilename; ///< Input roadmap mapfile
		string m_inputPathFile1; ///< Input roadmap path 
		/*string m_inputPathFile2; ///< Input roadmap path
		  string m_inputPathFile3; ///< Input roadmap path
		  GraphType* g3;
		  GraphType* g2;*/
		GraphType* g1;
		string m_connectorLabels;
		vector<pair<string,tuple<int, int, double> > > ranks; ///< stores the ranked paths

		PRMQuery<MPTraits>* m_query{nullptr}; ///< The query object.
};

template<class MPTraits>
CfgMasking<MPTraits>::
CfgMasking(/*string _inputMapFilename,*/ string _inputPathFilename1, 
		/*string _inputPathFilename2, string _inputPathFilename3,*/
		string _connectorLabels, vector<string> _evaluatorLabels) :
	/*m_inputMapFilename(_inputMapFilename),*/ m_inputPathFile1(_inputPathFilename1), 
	/*m_inputPathFile2(_inputPathFilename2), m_inputPathFile3(_inputPathFilename3),*/ m_connectorLabels(_connectorLabels){
		this->m_meLabels = _evaluatorLabels;
		this->SetName("CfgMasking");
	}


template<class MPTraits>
CfgMasking<MPTraits>::
CfgMasking(typename MPTraits::MPProblemType* _problem, XMLNode& _node) :
	MPStrategyMethod<MPTraits>(_problem, _node), /*m_inputMapFilename(""),*/ 
	m_inputPathFile1("")/*, m_inputPathFile2(""), m_inputPathFile3("")*/ {
		this->SetName("CfgMasking");
		ParseXML(_node);
	}

template<class MPTraits>
void
CfgMasking<MPTraits>::
ParseXML(XMLNode& _node) {
	//m_inputMapFilename = _node.Read("inputMap", false, "", "filename of roadmap to start from");
	m_inputPathFile1 = _node.Read("inputPath1", true, "", "filename of the path");
	//m_inputPathFile2 = _node.Read("inputPath2", true, "", "filename of the path");
	//m_inputPathFile3 = _node.Read("inputPath3", true, "", "filename of the path");

	for(auto& child : _node) {
		if(child.Name() == "Connector")
			m_connectorLabels = child.Read("method", true, "", "Connector Label");
		else if(child.Name() == "Evaluator")
			this->m_meLabels.push_back(child.Read("method", true, "", "Evaluator Label"));
	}
}

template<class MPTraits>
void
CfgMasking<MPTraits>::
Print(ostream& _os) const {
	MPStrategyMethod<MPTraits>::Print(_os);
	//_os << "\tInput Map: " << m_inputMapFilename << endl;
	_os << "\tPath 1: " << m_inputPathFile1 << endl;
	//_os << "\tPath 2: " << m_inputPathFile2 << endl;
	//_os << "\tPath 3: " << m_inputPathFile3 << endl;

	_os << "\tConnectors:: " << m_connectorLabels << endl;

	_os<<"\tMapEvaluators" << endl;
	for(const auto& label : this->m_meLabels)
		_os << "\t\t" << label << endl;


}

template<class MPTraits>
void
CfgMasking<MPTraits>::
Initialize() {
	if(this->m_debug)
		cout << "Initializing " << this->GetNameAndLabel() << "..." << endl;

	GraphType* g = this->GetRoadmap()->GetGraph();

	// Check for query info.
	m_query = nullptr;

	// If a query was loaded, process query cfgs
	if(this->UsingQuery()) {
		m_query = static_cast<PRMQuery<MPTraits>*>(this->GetMapEvaluator("PRMQuery").
				get());
		const vector<CfgType>& queryCfgs = m_query->GetQuery();
		g->AddVertex(queryCfgs.front());
	}

	//read in and reload roadmap and evaluators
	//	if(!m_inputMapFilename.empty()){
	//		RoadmapType* r = this->GetRoadmap();
	//		if(this->m_debug)
	//			cout << "\tLoading roadmap from \"" << m_inputMapFilename << "\".";
	//
	//		r->Read(m_inputMapFilename.c_str());
	//
	//		GraphType* g = r->GetGraph();
	//		for(typename GraphType::VI vi = g->begin(); vi != g->end(); ++vi)
	//			VDAddNode(g->GetVertex(vi));
	//		if(this->m_debug) {
	//			cout << "\tRoadmap has " << g->get_num_vertices() << " nodes and "
	//				<< g->get_num_edges() << " edges." << endl;
	//			cout << "\tResetting map evaluator states." << endl;
	//		}

	if(!m_inputPathFile1.empty()) {
		RoadmapType* r1 = new RoadmapType();
		if(this->m_debug)
			cout << "\tLoading roadmap from \"" << m_inputPathFile1 << "\".";

		r1->Read(m_inputPathFile1.c_str());
		g1 = r1->GetGraph();
		for(typename GraphType::VI vi = g1->begin(); vi != g1->end(); ++vi)
			VDAddNode(g1->GetVertex(vi));
		if(this->m_debug) {
			cout << "\tRoadmap has " << g1->get_num_vertices() << " nodes and "
				<< g1->get_num_edges() << " edges." << endl;
			cout << "\tResetting map evaluator states." << endl;
		}
	}
	//	if(!m_inputPathFile2.empty()) {
	//		RoadmapType* r2 = new RoadmapType();
	//		if(this->m_debug)
	//			cout << "\tLoading roadmap from \"" << m_inputPathFile2 << "\".";

	//		r2->Read(m_inputPathFile2.c_str());
	//		g2 = r2->GetGraph();
	//		for(typename GraphType::VI vi = g2->begin(); vi != g2->end(); ++vi)
	//			VDAddNode(g2->GetVertex(vi));
	//		if(this->m_debug) {
	//			cout << "\tRoadmap has " << g2->get_num_vertices() << " nodes and "
	//				<< g2->get_num_edges() << " edges." << endl;
	//			cout << "\tResetting map evaluator states." << endl;
	//		}
	//	}
	//	if(!m_inputPathFile3.empty()) {
	//		RoadmapType* r3 = new RoadmapType();
	//		if(this->m_debug)
	//			cout << "\tLoading roadmap from \"" << m_inputPathFile3 << "\".";

	//		r3->Read(m_inputPathFile3.c_str());
	//		g3 = r3->GetGraph();
	//		for(typename GraphType::VI vi = g3->begin(); vi != g3->end(); ++vi)
	//			VDAddNode(g3->GetVertex(vi));
	//		if(this->m_debug) {
	//			cout << "\tRoadmap has " << g3->get_num_vertices() << " nodes and "
	//				<< g3->get_num_edges() << " edges." << endl;
	//			cout << "\tResetting map evaluator states." << endl;
	//		}
	//	}

	for(const auto& label: this->m_meLabels) {
		MapEvaluatorPointer evaluator = this->GetMapEvaluator(label);
		if(evaluator->HasState())
			evaluator->operator()();
	}

	//}
}

template<class MPTraits>
void
CfgMasking<MPTraits>::
Run() {

	GraphType* g = this->GetRoadmap()->GetGraph();
	VID cfgNode;
//	typename GraphType::VI it1 = g->begin();
//	//CfgType node = g->GetVertex(itr);
//	int m = g->GetVertex(it1).DOF(); //getting dimensionality for this C-space
//	cout<<"m: "<<m<<endl;
//	typename GraphType::VI it2 = g1->begin();
//	//node = g1->GetVertex(itr);
//	int n = g1->GetVertex(it2).DOF(); // getting dimensionality of path
//	cout<<"n: "<<n<<endl;
//	VID cfgNode;
//
//	//transformation matrix
//	double tf[n][m];
//	for(int j =0; j <n; j++){
//		for(int k = 0; k<m; k++){
//			if(j == k)
//				tf[j][k] = 1.0;
//			else
//				tf[j][k] = 0.0;
//		}
//	}

	StatClass* stats = this->GetStatClass();
	string clock = "Projection Time";
	stats->StartClock(clock);

	//Path validation
	for(typename GraphType::VI pi = g1->begin(); pi != g1->end(); ++pi){
		/*if(pi == g1->begin())
			continue;*/
		//CfgType cfg = g1->GetVertex(pi);
		//cout<<"path node: "<<cfg<<endl;
		//vector<double> v = cfg.GetData();	
		//int i = 0;
		//double* arr = new double[n];
		//for(typename vector<double>::iterator vi = v.begin(); vi != v.end(); ++vi){
		//	arr[i] = *vi;
		//	//cout<<"Cfg data: "<<arr[i]<<endl;
		//	i++;
		//}

		//// projection of n configurations to m configurations
		//double* conv = new double[m];
		//for(int k =0; k <m; k++){
		//	for(int j = 0; j<n; j++){
		//		conv[k] += arr[j] * tf[j][k];
		//	}
		//}

		//// store as this space's configuration
		////vector<double> v;
		//v.clear();
		//v.reserve(m);
		//for(int k=0; k <m; k++){
		//	v.push_back(conv[k]);
		//}

		////Get transformation and add to graph g
		//CfgType vertex;
		//vertex.SetData(v);
		//cout<<"CFG: "<<vertex<<endl;
		cfgNode = g->AddVertex(g1->GetVertex(pi));
		if(cfgNode > 0)
			cout<<"Vertex ID: "<<cfgNode<<endl;

	}

	this->GetConnector(m_connectorLabels)->Connect(this->GetRoadmap(), g->begin(), g->end());
	bool mapPassedEvaluation = this->EvaluateMap();
	if(!mapPassedEvaluation)
		cout<<"The path is not valid"<<endl;

	stats->StopClock(clock);			
}

template<class MPTraits>
void
CfgMasking<MPTraits>::
Finalize() {
	// Output final map.
	this->GetRoadmap()->Write(this->GetBaseFilename() + ".map", this->GetEnvironment());

	// Output stats.
	ofstream  osStat(this->GetBaseFilename() + ".stat");
	StatClass* stats = this->GetStatClass();
	stats->PrintAllStats(osStat, this->GetRoadmap());

}

#endif
