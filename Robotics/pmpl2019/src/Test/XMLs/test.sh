#!/bin/bash

echo "Automation Started"

#sampler[0]='HybridPRM'
#sampler[1]='HPS'
#sampler[2]='UtilityGuidedGenerator'

#environments=('3D/MazeTunnel/mazetunnel') #3D/ZTunnel/tunnel') #'3D/Clutter/Heterogeneous/cube-0.5/clutteredCube') #'OpenChain/TunnelRegions/tunnelRegions') #'3D/Kuka/KukaYouBot') #'3D/Empty/empty')
#echo ${environments[0]}

#i=1

#for samp in ${sampler[@]}
#do
#for env in ${environments[@]}
#do
#((++i))
for run in {12345671..12345680}
do
#seed="$(shuf -i 12345670-12345680 -n 1)"

#file_name="${samp}_maze_${run}.xml"
#cp "BasicPRM.0.xml" ${file_name}
file_name="het_obst_${run}.xml"
cp "BasicPRM.0.xml" ${file_name}

#file_name="VisibilityPRM.xml"

#xmlstarlet edit -L -u "/MotionPlanning/Input/Environment/@filename" -v "${env}.env" ${file_name}
#xmlstarlet edit -L -u "/MotionPlanning/Input/Query/@filename" -v "${env}.query" ${file_name}
#xmlstarlet edit -L -u "/MotionPlanning/MPProblem/MPStrategies/BasicPRM/Connector/@method" -v "Closest" ${file_name}
#xmlstarlet edit -L -u "/MotionPlanning/MPProblem/Solver/@mpStrategyLabel" -v "${samp}" ${file_name}
#xmlstarlet edit -L -u "/MotionPlanning/MPProblem/Solver/@baseFilename" -v "${samp}_maze" ${file_name}
xmlstarlet edit -L -u "/MotionPlanning/MPProblem/Solver/@seed" -v "${run}" ${file_name}

#../../pmpl2017/src/pmpl -f ${file_name}
#echo "Completed ${file_name}"

#done
#done
done
echo "Automation Ended"

