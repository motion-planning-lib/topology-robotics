There are 2 folders in this files.

"maximal_cliques" is used to find the maximal cliques in a graph. 

	To use it, you need to put the map file (with current format) in the "data" folder inside.
	To run, using: "./find.sh >> output_file_name". 

"processing_data" is used to process the input map.

	The "map_input" converts map file to appropriate input for "maximal_cliques" package.
	To run, using: "./map_input map_file_name number_of_non_int_column >> output_file_name".

	The "output_map" converts the maximal cliques to the map file format.
	To run, using: "./output_map map_file_name max_clique_file number_of_non_int_column >> output_file_name"

	The "bi_rep" converts the maximal cliques into their binary representation.
	To run, using: " ./bi_rep max_clique_file number_of_nodes >> output_file_name"

	The "bi_xor" performs XOR operation and output the nodes index. 
	For now, since i'm using bitset (which requires the length of the bitmap at the start), 
	i have to hard code the number of node, so you'll need to change it accordingly to the input file (const int numNodes).
	Since i dont want to read the input file twice, I also hard code the number of line (1000 for now, change if larger).	
	To run, using: "./bi_xor binary_rep_file >> output_file_name"
	
	The "bi_output_map" convert the nodes index file (getting from the binary representation) to the map file format.
	To run, using: " ./bi_output_map map_file_name node_index_file number_of_non_int_column >> output_file_name"

For kuka_map, the command would be:

	"./map_input kukafalse.map.0.map 8 >> graph.txt"
	
	"./find.sh >> max_cliques.txt". 

	"./output_map kukafalse.map.0.map max_cliques.txt 8 >> clique_map.map"

	"./bi_rep max_cliques.txt 500 >> kuka_bi_rep.txt"
	
	"./bi_xor kuka_bi_rep.txt >> kuka_bi_xor.txt"
	
	"./bi_output_map kukafalse.map.0.map kuka_bi_xor.txt 8 >> new_kuka.map"
	

