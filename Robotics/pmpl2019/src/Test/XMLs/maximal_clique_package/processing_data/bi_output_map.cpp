/*
 * input: original map file, binary xor file with node index, non-integer-format columns in the input file 
 * output: map file
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

int main (int argc, char *argv[]) {
  
  //number of non-integer-format columns in the input file
	int numStr = atoi(argv[3]);	

  ifstream myfile(argv[1]);
  if (myfile.is_open()) {
	string dummyLine, line1, line2, line3, line4;
	string line;
	string finalLine = "#####GRAPHSTOP#####";

  char c;					// testing
  myfile.get(c); //cout << c << "\n" <<endl;	// testing

	getline(myfile, line1); //read line1
	getline(myfile, line2); //read line2
	getline(myfile, line3); //read line3
	getline(myfile, line4); //read line4
	
	int arr[2];
	int i = 0;
	//Get number of nodes and edges	
	getline(myfile, dummyLine);
	stringstream ssin(dummyLine);
	while (ssin.good()){
               ssin >> arr[i];
               ++i;
               }
    int numNodes = arr[0]; //cout << numNodes << '\n';
    int numEdges = arr[1]; //cout << numEdges << '\n';
    
    //int graph[numNodes][numNodes];
    int **graph = new int*[numNodes];
	//init the graph
	
	for (int i = 0; i <numNodes; i++) {
                graph[i] = new int[numNodes];
		for (int j = 0; j <numNodes; j++) {
			graph[i][j] = 0;
		}
	}
	
	int startNode = 0;
	
	string cor[numNodes][numStr]; 

    while(getline(myfile,line).good() && (startNode < numNodes))
    { //if(line.length() == 0) continue;

	  string arr[1000];	
	  int n = 0;
	  
	  //convert each line to string array
	  stringstream ssin(line);
	  while (ssin.good()){
		ssin >> arr[n];
		++n;
      }
      
      for (int i = 0; i < numStr; i++ ){
		cor[startNode][i] = arr[i+2];	  
	  }
      
      //convert string array to integer array
      int intArr[n];
      for (int i = 0; i < n; i ++) {
		intArr[i] = atoi (arr[i].c_str());
	  }
		
	  //Update the graph, change index according to map
	  int index = numStr + 3; 
	  while(index < n-1) {
		int endNode = intArr[index];
		int weight = intArr[index+2];
		//cout << intArr[index] << endl;
		graph[startNode][endNode] = weight; 
		index = index + 3;
	  }
		
		startNode++;
    } 
   
    myfile.close();
	
	cout << line1 << endl;
	cout << line2 << endl;
	cout << line3 << endl;
	cout << line4 << endl;
	
	int numEdgeFinal = 0;
	int nodeStart = 0;	
     
    int newNode = 0;
    //int node[numNodes];
    int *node = new int[numNodes];
	
	ifstream myfile2(argv[2]); 
          
    if (myfile2.is_open()) {
		string newLine;
		
		while (getline (myfile2,newLine).good()){
			node[newNode] = atoi(newLine.c_str());	
			newNode++;
			//cout<<"|" << newNode<<endl;
		}
       		
	}
	else cout << "Unable to open nodes index file." << endl;
     
	myfile2.close();
    
	cout << newNode << " " << "0" << " " << newNode << endl;
	
	//Output
	for (int i = 0; i < newNode; i++) {
		for (int j = 0; j < numNodes; j++) {
			//cout << j << " : " << node[i] << endl;
			if (node[i] == j) { // check index
				cout << i << "    " << "0" << "  ";
				//cout << node[i] << endl;
		
				for (int k = 0; k < numStr; k++){
					cout << cor[j][k] << "   ";
				}

				cout << "0" << " ";
		
				cout << endl;
				break;
			}
		}    	
	}
       
    cout << finalLine << endl;
  }

  else cout << "Unable to open map file." << endl;

  return 0;
}
