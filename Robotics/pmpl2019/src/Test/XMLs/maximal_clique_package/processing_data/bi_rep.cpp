/*
 *input: maximal cliques file, node index of the binary XOR file
 *output: binary representation of the maximal cliques file
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

int main (int argc, char *argv[]) {
	
	int numNodes = atoi(argv[2]);
	
	ifstream myfile(argv[1]);
	
	if (myfile.is_open()) {
		string line;
		string dummyLine;
		getline(myfile, dummyLine);
		getline(myfile, dummyLine);
		
		while (getline (myfile,line)){
			
			int a[100] = { 0 };	
			int m = 0;
	  
			//convert each line to int array
			stringstream ssin(line);
			while (ssin.good()){
				ssin >> a[m];
				//cout << a[m] << " ";
				++m;
			}
			
			int out[numNodes]; // init
			for (int i = 0; i < numNodes; i++){
				out[i] = 0;
			}
					
			//binary_rep
			for (int i = 0; i < m; i++){
				for (int j = 0; j < numNodes; j++) {
					if (a[i] == j){
						out[j] = 1;
					}
				}
			}	
			
			//output
			for (int i = 0; i < numNodes; i++){
				cout << out[i];
			}						
			
			cout << endl;
		}	   
	}
  
	else cout << "Unable to open maximal clique file." << endl; 
  	
	
	return 0;
}
