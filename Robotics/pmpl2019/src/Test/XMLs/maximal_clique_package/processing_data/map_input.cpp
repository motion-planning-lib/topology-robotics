//
// input: original map file, non-integer-format columns in the input file
// output: maximal cliques (one line per clique)
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

int main (int argc, char *argv[]) {
	
  string line;
  
  //number of non-integer-format columns in the input file
  int numStr = atoi(argv[2]);
  
  ifstream myfile(argv[1]);
  if (myfile.is_open())
  {
	string dummyLine;
	getline(myfile, dummyLine); //skip line
	getline(myfile, dummyLine); //skip line
	getline(myfile, dummyLine); //skip line
	getline(myfile, dummyLine); //skip line
	
	int arr[2];
	int i = 0;
	//Get number of nodes and edges
	getline(myfile, dummyLine);
	stringstream ssin(dummyLine);
	while (ssin.good()){
             ssin >> arr[i];
             ++i;
        }
    int numNodes = arr[0]; //cout << numNodes << '\n';
    int numEdges = arr[1]; //cout << numEdges << '\n';
   
    int **graph = new int*[numNodes]; 
    //int graph[numNodes][numNodes];
    
	//init the graph
	for (int i = 0; i <numNodes; i++) {
                graph[i] = new int[numNodes];
		for (int j = 0; j <numNodes; j++) {
			graph[i][j] = 0;
		}
	}
	
	int startNode = 0;
	
    while(getline (myfile,line) && (startNode < numNodes))
    {     
	  string arr[1000];	
	  int n = 0;
	   
	  //convert each line to string array
	  stringstream ssin(line);
	  while (ssin.good()){
               	ssin >> arr[n];
                ++n;
          }
      
     //convert string array to integer array
      int intArr[n];
      
      for (int i = 0; i < n; i ++) {
                intArr[i] = atoi(arr[i].c_str());
                  //cout << intArr[i] << '\n';
	  }

	  //Update the graph; change index according to the map file 

	  int index = numStr +3;
	  while(index < n-1) {
		int endNode = intArr[index];
		//cout << intArr[index] << endl;
		graph[startNode][endNode] = 1; 
		index = index + 3;
	  }
		
		startNode++;
    }
    
    myfile.close();

	
	//Convert to undirected graph
	for (int i = 0; i <numNodes; i++) {
		for (int j = 0; j <numNodes; j++) {
			if(graph[i][j] == 1 && graph[j][i] == 0)
			   graph[j][i] = 1;
		}
	}	
	
	//Count number of edges
	int total = 0;
	for (int i = 0; i <numNodes; i++) {
		for (int j = 0; j <numNodes; j++) {
			if(graph[i][j] == 1)
			  total++;
		}
	}
	
	//Output
	cout << numNodes << endl;
	cout << total << endl;
	
	for (int i = 0; i <numNodes; i++) {
		for (int j = 0; j <numNodes; j++) {
			if(graph[i][j] == 1)
			   cout << i << "," << j << endl;
		}
	}
  }

  else cout << "Unable to open map file"; 
  
  return 0;
}
