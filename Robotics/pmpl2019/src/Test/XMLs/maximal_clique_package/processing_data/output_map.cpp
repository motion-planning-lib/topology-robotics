//
// input: original map file, maximal cliques file
// output: map file
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

int main (int argc, char *argv[]) {
	
 
  
  //number of non-integer-format columns in the input file
  int numStr = atoi(argv[3]);	

  ifstream myfile(argv[1]);
  if (myfile.is_open()) {
	string dummyLine, line1, line2, line3, line4;
	string line;
	string finalLine = "#####GRAPHSTOP#####";
	getline(myfile, line1); //read line1
	getline(myfile, line2); //read line2
	getline(myfile, line3); //read line3
	getline(myfile, line4); //raed line4
	
	int arr[2];
        int i = 0;
	//Get number of nodes and edges
	getline(myfile, dummyLine);
	stringstream ssin(dummyLine);
	while (ssin.good()){
      ssin >> arr[i];
      ++i;
    }
    int numNodes = arr[0]; //cout << numNodes << '\n';
    int numEdges = arr[1]; //cout << numEdges << '\n';
    
    int **graph = new int*[numNodes];
    //int graph[numNodes][numNodes];
	//init the graph
	for (int i = 0; i <numNodes; i++) {
                graph[i] = new int[numNodes];
		for (int j = 0; j <numNodes; j++) {
			graph[i][j] = 0;
		}
	}
	
	int startNode = 0;
	
	string cor1[numNodes];
	string cor2[numNodes];
	string cor[numNodes][numStr];
	
    while ( getline (myfile,line) && (startNode < numNodes))
    {
	  string arr[1000];	
	  int n = 0;
	  
	  //convert each line to string array
	  stringstream ssin(line);
	  while (ssin.good()){
		ssin >> arr[n];
		++n;
      }
      
      for (int i = 0; i < numStr; i++ ){
		cor[startNode][i] = arr[i+2];	  
	  }
      
      cor1[startNode] = arr[2];
      cor2[startNode] = arr[3];
      
      //convert string array to integer array
      int intArr[n];
      for (int i = 0; i < n; i ++) {
		intArr[i] = atoi (arr[i].c_str());
		//cout << intArr[i] << '\n';
	  }
		
	  //Update the graph, change index according to map
	  //int index = 11;
	  int index = numStr + 3; 
	  while(index < n-1) {
		int endNode = intArr[index];
		int weight = intArr[index+2];
		//cout << intArr[index] << endl;
		graph[startNode][endNode] = weight; 
		index = index + 3;
	  }
		
		startNode++;
    }
    myfile.close();
	
	
	int total = 0;
	for (int i = 0; i <numNodes; i++) {
		for (int j = 0; j <numNodes; j++) {
			if(graph[i][j] != 0)
			   total++;
		}
	}
	
	cout << line1 << endl;
	cout << line2 << endl;
	cout << line3 << endl;
	cout << line4 << endl;
	
	int numEdgeFinal = 0;
	int nodeStart = 0;
	int count = 0;
	//int cliqueGraph[numNodes][numNodes];
	int **cliqueGraph = new int*[numNodes];
	
    ifstream myfile2(argv[2]);
    if (myfile2.is_open()) {
		string line;
		string dummyLine;
		getline(myfile2, dummyLine);
		getline(myfile2, dummyLine);
		

		//init
		for (int i = 0; i <numNodes; i++) {
                        cliqueGraph[i] = new int[numNodes];
			for (int j = 0; j <numNodes; j++) {
				cliqueGraph[i][j] = 0;
			}
		}		
		
		while (getline (myfile2,line)){
			int a2[100] = { 0 };	
			int m = 0;
	  
			//convert each line to int array
			stringstream ssin(line);
			while (ssin.good()){
				ssin >> a2[m];
				//cout << m << " : " << a2[m] << endl;
				++m;
			}
			
			for (int i = 0; i < m; i++){
				for (int j = i+1; j < m; j++) {
					int s = a2[i];
					int e = a2[j];
					if (cliqueGraph[s][e] == 0) {
						cliqueGraph[s][e] = graph[s][e];
					}
				}
			}		
		}	 
	}
	else cout << "Unable to open maximal clique file." << endl;
    
    //Count number of edges
    for (int i = 0; i <numNodes; i++) {
		for (int j = 0; j <numNodes; j++) {
			if(cliqueGraph[i][j] != 0)
				count++;
		}
	}
    
    cout << numNodes << " " << count << " " << numNodes << endl;
	
	
	//Output
	for (int i = 0; i < numNodes; i++) {
		cout << i << "    " << "0" << "  ";
		//cout << cor1[i] << "   " << cor2[i] << "   ";
		
		for (int k = 0; k < numStr; k++){
			cout << cor[i][k] << "   ";
		}
			
		int co_e = 0;
			
		for (int j = 0; j < numNodes; j++) {
			if (cliqueGraph[i][j] != 0) {
				co_e++;
			}
		}
			
		cout <<  co_e << " ";
		
		for (int j = 0; j < numNodes; j++) {
			if (cliqueGraph[i][j] != 0) {
				cout << j << " 0 " << cliqueGraph[i][j] << "  ";
			}
		}
		cout << endl;
		}    
    
    
    cout << finalLine << endl;
  }

  else cout << "Unable to open map file." << endl; 

  return 0;
}
