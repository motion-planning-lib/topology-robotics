/*
 * input: binary XOR file
 * output: node index of the binary XOR file
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>
#include <bits/stdc++.h>
#include <bitset>
using namespace std;
const int numNodes = 20000; //Change according to number of nodes

int main (int argc, char *argv[]) {

	//cout<<"start"<<endl;
        //const int numNodes = atoi(argv[2]);
	bitset<numNodes> result(0);
        ifstream myfile(argv[1]);
	if (myfile.is_open()) {
		string line;
	        char c;
	        myfile.get(c);	
		int bitIndex = 0;
		
		int numLine = 10000000; // change according to the input file.
		bitset<numNodes> *bit = new bitset<numNodes>[numLine];
		//bitset<numNodes> bit[numLine];
		
		while (getline (myfile,line)){	
			string a[100];	
			int m = 0;
	  
			//convert each line to string array
			stringstream ssin(line);
			while (ssin.good()){
				ssin >> a[m];
				//cout << a[m] << " ";
				++m;
			}		
					
			bit[bitIndex] = bitset<numNodes>(a[m-1]);
			//cout << bit[bitIndex] << endl;
			bitIndex++;
		}
		
		//Do bitwise XOR
		for (int i = 0; i < bitIndex; i++){
			result = result ^= bit[i];
		}
		//cout << result << endl;
		
		string out = result.to_string();
		
		//Print out node indexes
		for(int i = 0; i<out.length(); i++) {
			if(out[i] == '1')
				cout << i << endl;
		}
		
		myfile.close();
	}
	else cout << "Unable to open maximal clique file." << endl; 

 return 0;

}
