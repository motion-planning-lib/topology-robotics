<?xml version='1.0' encoding='UTF-8'?>
<MotionPlanning warnings="true" warningsAsErrors="true" print="false">
  <Input>
    <Environment filename="3DState/3d.env"/>
    <Query filename="3DState/3d.query"/>
  </Input>

  <MPProblem>
    <DistanceMetrics>
      <WeightedEuclidean label="weuclidean"
        posWeight="0.4" rotWeight="0.4" velWeight="0.1" avlWeight="0.1"/>
      <WeightedEuclidean label="weuclidean-pos"
        posWeight="0.5" rotWeight="0.5" velWeight="0" avlWeight="0"/>
      <ReachabilityDistance label="reachableDistance"/>
    </DistanceMetrics>

    <ValidityCheckers>
      <CollisionDetection label="rapid" method="RAPID"/>
      <CollisionDetection label="pqp_solid" method="PQP_SOLID"/>
      <AlwaysTrueValidity label="alwaysTrue"/>
    </ValidityCheckers>

    <NeighborhoodFinders>
      <BruteForceNF label="Nearest"
        dmLabel="weuclidean" unconnected="false" k="1"/>
      <BruteForceNF label="ReachableNF"
        dmLabel="reachableDistance" unconnected="false" k="1"/>
      <BruteForceNF label="NearestPosition"
        dmLabel="weuclidean-pos" unconnected="false" k="1"/>
    </NeighborhoodFinders>

    <Samplers>
      <UniformRandomSampler label="UniformRandom" vcLabel="alwaysTrue"/>
    </Samplers>

    <Extenders>
      <KinodynamicExtender label="FixedBest" dmLabel="weuclidean" vcLabel="rapid"
        timeStep="30" fixed="true" best="true" minDist=".01"/>
      <KinodynamicExtender label="FixedRandom" dmLabel="weuclidean"
        vcLabel="rapid" timeStep="30" fixed="true" best="false" minDist=".01"/>
      <KinodynamicExtender label="VariBest" dmLabel="weuclidean" vcLabel="rapid"
        timeStep="30" fixed="false" best="true" minDist=".01"/>
      <KinodynamicExtender label="VariRandom" dmLabel="weuclidean" vcLabel="rapid"
        timeStep="30" fixed="false" best="false" minDist=".01"/>

      <MixExtender label="Schmorgasbord">
        <Extender label="FixedRandom" probability=".5"/>
        <Extender label="FixedBest" probability="1"/>
        <Extender label="VariRandom" probability=".5"/>
        <Extender label="VariBest" probability="1"/>
      </MixExtender>
    </Extenders>

    <Metrics>
      <NumNodesMetric label="NumNodes"/>
    </Metrics>

    <MapEvaluators>
      <ConditionalEvaluator label="NodesEval" metric_method="NumNodes"
        value="100" operator=">="/>
      <RRTQuery label="RRTQuery" goalDist=".3"
        nfLabel="NearestPosition" exLabel="FixedRandom" debug="false"/>
      <ComposeEvaluator label="BoundedRRTQuery" operator="OR">
        <Evaluator label="RRTQuery"/>
        <Evaluator label="NodesEval"/>
      </ComposeEvaluator>
    </MapEvaluators>

    <MPStrategies>
      <!-- KinodynamicRRT -->
      <BasicRRTStrategy label="RRT" growthFocus="0.05"
        dmLabel="weuclidean" nfLabel="Nearest" vcLabel="rapid"
        gtype="UNDIRECTED_TREE" extenderLabel="FixedRandom" debug="false">
        <Evaluator label="BoundedRRTQuery"/>
      </BasicRRTStrategy>

      <!-- DynamicDomainRRT -->
      <DynamicDomainRRT label="DynamicDomainRRT" growthFocus="0.05"
        dmLabel="weuclidean" nfLabel="Nearest" vcLabel="rapid"
        gtype="UNDIRECTED_TREE" extenderLabel="FixedRandom" debug="false" r="2">
        <Evaluator label="BoundedRRTQuery"/>
      </DynamicDomainRRT>

      <!-- DynamicRegionRRT -->
      <DynamicRegionRRT label="DynamicRegionRRT" growthFocus="0.05"
        dmLabel="weuclidean" nfLabel="Nearest" vcLabel="rapid"
        gtype="UNDIRECTED_TREE" extenderLabel="FixedRandom" debug="false">
        <Evaluator label="BoundedRRTQuery"/>
      </DynamicRegionRRT>

      <!-- Syclop -->
      <Syclop label="Syclop" growthFocus="0.05"
        dmLabel="weuclidean" nfLabel="Nearest" vcLabel="rapid"
        gtype="UNDIRECTED_TREE" extenderLabel="FixedRandom" debug="false">
        <Evaluator label="BoundedRRTQuery"/>
      </Syclop>
    </MPStrategies>

    <Solver mpStrategyLabel="RRT" seed="12345678" baseFilename="RRT"/>

  </MPProblem>
</MotionPlanning>
