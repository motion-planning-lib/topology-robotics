<?xml version='1.0' encoding='UTF-8'?>
<MotionPlanning warnings="true" warningsAsErrors="true" print="false">
  <MPProblem>

    <Environment filename="2D/2d.env" saveDofs="false"/>

    <DistanceMetrics>
      <Euclidean label="euclidean"/>
      <ScaledEuclidean label= "scaledEuclidean" scale ="0.5"/>
      <!--CenterOfMass label="com"/-->
      <!--KnotTheory label="knot"/-->
      <!--Manhattan label="manhattan"/-->
      <!--Minkowski label="minkowski" r1="3.0" r2="3.0" r3="0.333" normalize="false"/-->
      <!--RMSD label="rmsd"/-->
      <!--LPSwept label="LPSwept" posRes="5" oriRes="5" useBBox="true" lpLabel="sl"/-->
      <!--BinaryLPSwept label="BLPSwept" posRes="5" oriRes="5" useBBox="true" lpLabel="sl"/-->
    </DistanceMetrics>

    <ValidityCheckers>
      <CollisionDetection label="rapid" method="RAPID"/>
      <CollisionDetection label="pqp" method="PQP"/>
      <CollisionDetection label="pqp_solid" method="PQP_SOLID"/>
      <!--NodeClearanceValidity label="nc" delta="0.3" nfLabel="Nearest"/-->
      <!--NegateValidity label="nv" vcLabel="rapid"/-->
      <!--MedialAxisClearance label="mac" maClearance="0.5" vcLabel="pqp" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true" positional="true"
        epsilon="0.01" historyLength="5"/-->
      <!--ComposeValidity label="cv" operator="AND">
        <ValidityChecker label="pqp"/>
        <ValidityChecker label="mac"/>
      </ComposeValidity-->
      <AlwaysTrueValidity label="alwaysTrue"/>
      <!--ObstacleClearance label="oc" obstClearance="2.5" vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true" positional="true"/-->
    </ValidityCheckers>

    <NeighborhoodFinders>
      <BruteForceNF label="BFNF" dmLabel="euclidean" unconnected="false" k="10"/>
      <BruteForceNF label="Nearest" dmLabel="euclidean" unconnected="false" k="1"/>
      <BruteForceNF label="BFNFAll" dmLabel="euclidean" k="0"/>
      <!--HierarchicalNF label="HierarchicalNF" nfLabel="BFNF" nfLabel2="RandomNF" dmLabel=""/-->
      <!--HopLimitNF label="HLNF" hoplimit="2" dmLabel="" nfLabel="BFNF"/-->
      <!--OptimalNF label="OptimalK" nfLabel="BFNF"/-->
      <RadiusNF label="RadiusNF" dmLabel="euclidean" radius="5"/>
      <!--RandomNF label="RandomNF" dmLabel="euclidean" k="5"/-->
      <!--DPESNF label="BFNF" dmLabel="euclidean"/-->
      <!--MPNNNF label="BFNF" dmLabel="euclidean" use_rotational="0" use_scaling="0"/-->
      <!--CGALNF label="CGALNF" dmLabel="euclidean"/-->
      <!--SpillTreeNF label="BFNF" dmLabel="euclidean"/-->
      <!--MetricTreeNF label="BFNF" dmLabel="euclidean"/-->
      <!--BandsNF label="RbandNF_2bands_4k" debug="false" dmLabel="euclidean">
        <RBand label="first" max = '8' k='8' policy='closest' dmLabel="euclidean"/>
        <RBand label="second" min = '8' max = '64' k='8' policy='random' dmLabel="euclidean"/>
      </BandsNF>
      <BandsNF label="DbandNF" debug="false" dmLabel="euclidean">
        <DBand label="first" min='.1' max='.3' k='16' policy='closest' dmLabel="euclidean"/>
      </BandsNF-->
    </NeighborhoodFinders>

    <Samplers>
      <UniformRandomSampler label="UniformRandom" vcLabel="alwaysTrue"/>
      <UniformRandomSampler label="UniformRandomFree" vcLabel="rapid"/>
      <ObstacleBasedSampler label="OBPRM" vcLabel="rapid" useBBX="false" pointSelection="cspace"
        dmLabel="euclidean" stepSize="0.0" nShellsColl="0" nShellsFree="1" debug="false"/>
      <GaussianSampler label="Gauss" vcLabel="rapid" d="0.2" dmLabel="euclidean" useBBX="false"/>
      <BridgeTestSampler label="Bridge" vcLabel="rapid" d="0.2" dmLabel="euclidean" useBBX="true"/>
      <!--MedialAxisSampler label="MAPRM" vcLabel="pqp" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact"
        useBBX="true" debug="false" positional="true"
        epsilon="0.01" historyLength="100"/-->
      <!--GridSampler label="Grid" vcLabel="rapid" useBBX="true">
        <Dimension index="0" points="10"/>
        <Dimension index="1" points="5"/>
        <Dimension index="2" points="5"/>
      </GridSampler-->
      <!--MixSampler label="MixSampler">
        <Sampler label="UniformRandomFree" p="0.8"/>
        <Sampler label="UniformRandom" p="0.2"/>
      </MixSampler-->
      <!--UniformObstacleBasedSampler label="UOBPRM" vcLabel="rapid" useBBX="false" dmLabel="euclidean" d="0.02" /-->
      <!--UniformMedialAxisSampler label="UMAPRM" vcLabel="pqp_solid" useBBX="true" dmLabel="euclidean" d="2.00" t="10"
        clearanceType="exact" penetrationType="exact" debug="false" positional="true"/-->
    </Samplers>

    <LocalPlanners>
      <StraightLine label="sl" binaryEvaluation="true" vcLabel="rapid"/>
      <StraightLine label="slAlwaysTrue" binaryEvaluation="true" vcLabel="alwaysTrue"/>
      <!--RotateAtS label="ras" binaryEvaluation="true" vcLabel="rapid" s="0.5" saveIntermediates="false"/>
      <TransformAtS label="tas" binaryEvaluation="true" vcLabel="rapid" s="0.5" saveIntermediates="false"/-->
      <!--AStarDistance label="asd" vcLabel="rapid" dmLabel="euclidean"
        maxTries="6" numNeighbors="10" histLength="10" debug="true"/>
      <AStarClearance label="asc" vcLabel="pqp" maxTries="6" numNeighbors="3" histLength="10"
        dmLabel="euclidean" clearanceType="exact" penetrationType="exact"
        clearanceRays="2" penetrationRays="2" useBBX="true" positional="true" debug="false"/-->
      <!--MedialAxisLP label="malpr" controller="recursive"
        macEpsilon="0.1" maxIter="10"
        vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true"
        epsilon="0.01" historyLength="5" positional="true"
        debug="true" saveIntermediates="true"/>
      <MedialAxisLP label="malpi" controller="iterative"
        resFactor="20" maxIter="100"
        vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true"
        epsilon="0.01" historyLength="5" positional="true"
        debug="true" saveIntermediates="true"/>
      <MedialAxisLP label="malpb" controller="binary"
        resFactor="20" maxIter="100"
        vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true"
        epsilon="0.01" historyLength="5" positional="true"
        debug="true" saveIntermediates="true"/-->
      <!--ToggleLP label="togglelp" vcLabel="rapid" dmLabel="euclidean"
        lpLabel="sl" maxIter="5"/-->
      <!--ApproxSpheres label="apsph" vcLabel="pqp" dmLabel="euclidean" saveIntermediates="false"
        clearanceType="exact" penetrationType="exact"/-->
      <!--HierarchicalLP label="hlp">
        <LocalPlanner method="sl"/>
        <LocalPlanner method="ras"/>
        <LocalPlanner method="malp"/>
      </HierarchicalLP-->
    </LocalPlanners>

    <Extenders>
      <BasicExtender label="BERO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="1.0"/>
      <BasicExtender label="BESO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0" randomOrientation="false"/>
      <!--MixExtender label="ME">
        <Extender label="BERO" probability="0.1"/>
        <Extender label="BESO" probability="0.1"/>
        <Extender label="ROVRO" probability="0.1"/>
        <Extender label="ROVSO" probability="0.1"/>
        <Extender label="RTT" probability="0.1"/>
        <Extender label="TORO" probability="0.1"/>
        <Extender label="TOSO" probability="0.1"/>
        <Extender label="TCSO" probability="0.1"/>
        <Extender label="TMAP" probability="0.1"/>
      </MixExtender>
      <MedialAxisExtender label="MAE" delta="10.0" minDist="0.01"
        extendDist="0.5" maxIntermediates="10" lpLabel="sl"
        vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact" useBBX="true"
        epsilon="0.01" historyLength="5" positional="true"/>
      <RandomObstacleVector label="ROVRO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0"/>
      <RandomObstacleVector label="ROVSO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0" randomOrientation="false"/>
      <RotationThenTranslation label="RTT" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0"/>
      <TraceCSpaceObstacle label="TCSO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0"/>
      <TraceMAPush label="TMAP" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0"
        clearanceType="exact" penetrationType="exact"/>
      <TraceObstacle label="TORO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0"/>
      <TraceObstacle label="TOSO" debug="false"
        dmLabel="euclidean" vcLabel="rapid" delta="10.0" randomOrientation="false"/-->
    </Extenders>

    <!--PathModifiers>
      <CombinedPathModifier label="CombinedPathModifier" debug="true">
        <Modifier label="ShortcuttingPathModifier"/>
        <Modifier label="MedialAxisPathModifier"/>
        <Modifier label="ResamplePathModifier"/>
      </CombinedPathModifier>
      <CRetractionPathModifier label="CR" iter="25"
        vcLabel="pqp_solid" dmLabel="euclidean"
        clearanceType="exact" penetrationType="exact"
        useBBX="true" positional="true" debug="false"/>
      <MedialAxisPathModifier label="MAPS"
        pmLabel="SC" lpLabel="sl" malpLabel="malpi" debug="false"/>
      <ResamplePathModifier label="ResamplePathModifier" debug="false"
        typeName="MAX_CLEARANCE"
        numResamples="1" stepSize="0.3" userValue="0.3"
        vcLabel="pqp" dmLabel="euclidean" lpLabel="sl"
        clearanceType="exact" penetrationType="exact" useBBX="true" positional="true"/>
      <ShortcuttingPathModifier label="ShortcuttingPathModifier" lpLabel="sl"
        debug="false"/>
    </PathModifiers-->

    <Connectors>
      <!--AdaptiveConnector label="AdaptiveCS" nfLabel="" lpLabel="sl" percentRandom="0.5" checkIfSameCC="false" debug="true" uniformProbability="false" fixedCost="false" fixedReward="false">
        <NeighborFinder Method="BFNF"/>
        <NeighborFinder Method="Nearest"/>
        <NeighborFinder Method="RandomNF"/>
        <NeighborFinder Method="BFNFAll"/>
      </AdaptiveConnector-->
      <CCsConnector label="ConnectCCs" nfLabel="BFNF" lpLabel="sl" k="5"/>
      <!--ClosestVE label="ClosestVE" kClosest="3" nfLabel="BFNF"
           lpLabel="sl"/-->
      <NeighborhoodConnector label="Closest" nfLabel="BFNF" lpLabel="sl" checkIfSameCC="false" debug="false"/>
      <NeighborhoodConnector label="SparkPRMConnector" nfLabel="Nearest"
        lpLabel="sl" checkIfSameCC="true"/>
      <NeighborhoodConnector label="ToggleConnect" nfLabel="BFNF" lpLabel="sl"
        countFailures="true" fail="1" checkIfSameCC="false"/>
      <NeighborhoodConnector label="ClosestAlwaysTrue" nfLabel="BFNF" lpLabel="slAlwaysTrue" checkIfSameCC="false"/>
      <NeighborhoodConnector label="AllToAll" nfLabel="BFNFAll" lpLabel="sl" checkIfSameCC="false"/>
      <!--PreferentialAttachment label="Preferential" lp_method="sl" nf="BFNF" k="2" fail="5" unconnected="true"
        random="true" dm_method="euclidean" CheckIfSameCC="false"/-->
      <RegionConnector label="RegionConnector" nfLabel="BFNF" lpLabel="sl" numIters="4"/>
      <RegionRRTConnect label="RegionRRTConnect" nfLabel="BFNF" lpLabel="sl" eLabel="BERO" minDist="0.0" iterations="10000"/>
      <RewireConnector label="OptimalRewire" nfLabel="OptimalK" lpLabel="sl"/>
      <RRTConnect label="RRTConnect" nfLabel="BFNF" lpLabel="sl" eLabel="BERO" minDist="0.0" iterations="10000"/>
    </Connectors>

    <Metrics>
      <NumNodesMetric label="NumNodes"/>
      <!--NumEdgesMetric label="NumEdges"/-->
      <!--CoverageMetricRoadmapSet label="CoverageRoadmapSet" filename="2D/2d.map"
        computeAllCCs="false">
        <Connector label="Closest"/>
      </CoverageMetricRoadmapSet>
      <CoverageMetricVectorSet label="CoverageVectorSet" filename="coverage"
        computeAllCCs="false">
        <Connector label="Closest"/>
      </CoverageMetricVectorSet>
      <CoverageDistanceMetricRoadmapSet label="CoverageDistance"
        filename="2D/2d.map" dmLabel="euclidean"/>
      <ConnectivityMetricRoadmapSet label="ConnectivityRoadmapSet"
        filename="2D/2d.map">
        <Connector label="Closest"/>
      </ConnectivityMetricRoadmapSet>
      <ConnectivityMetricVectorSet label="ConnectivityVectorSet" filename="connectivity">
        <Connector label="Closest"/>
      </ConnectivityMetricVectorSet-->
      <!--DiameterMetric label="Diameter"/-->
      <!--CCDistanceMetric label="CCDistance" dmLabel="euclidean"/-->
      <!--TimeMetric label="TimeInSec"/-->
    </Metrics>

    <MapEvaluators>
      <!--TrueEvaluation label="TrueEvaluator" /-->
      <!--Query label="Query" queryFile="2D/2d.query" dmLabel="euclidean"
        lpLabel="sl" debug="false" pmLabel="ShortcuttingPathModifier">
        <NodeConnectionMethod method="Closest"/>
      </Query>
      <LazyQuery label="LazyQuery" queryFile="2D/2d.query"
        lpLabel="sl" dmLabel="euclidean" vcMethod="rapid" graphSearchAlg="astar"
        deleteNodes="false" debug="false"
        numEnhance="10" d="0.5" pmLabel="">
        <NodeConnectionMethod method="ClosestAlwaysTrue"/>
        <Resolution mult="27"/>
        <Resolution mult="16"/>
        <Resolution mult="1"/>
      </LazyQuery>
      <LazyToggleQuery label="LazyToggleQuery" queryFile="2D/2d.query"
        lpLabel="sl" dmLabel="euclidean" vcMethod="rapid" graphSearchAlg="astar"
        deleteNodes="false" iterative="true" debug="false" pmLabel="">
        <NodeConnectionMethod method="ClosestAlwaysTrue"/>
        <ToggleConnectionMethod method="LazyToggleConnect"/>
        <Resolution mult="27"/>
        <Resolution mult="16"/>
        <Resolution mult="1"/>
      </LazyToggleQuery-->
      <!--PrintMapEvaluation label="PrintMap" base_name="Basic"/-->
      <ConditionalEvaluator label="NodesEval" metric_method="NumNodes" value="100" operator=">="/>
      <ConditionalEvaluator label="EdgesEval" metric_method="NumEdges" value="1000" operator=">"/>
      <ConditionalEvaluator label="FreqNodeEval" metric_method="NumNodes" value="50" operator="%"/>
      <!--NegateEvaluator label="Neg" evalLabel="NodesEval"/-->
      <!--ComposeEvaluator label="Com1" operator="or">
        <Evaluator label="NodesEval"/>
        <Evaluator label="Query"/>
      </ComposeEvaluator>
      <ComposeEvaluator label="Com2" operator="and">
        <Evaluator label="Com1"/>
        <Evaluator label="PrintMap"/>
      </ComposeEvaluator>
      <ComposeEvaluator label="ModQuery" operator="and">
        <Evaluator label="FreqNodeEval"/>
        <Evaluator label="Query"/>
      </ComposeEvaluator-->
    </MapEvaluators>

    <!-- For **UAStrategy use** -->
    <!--partitioning_methods>
      <Kmeans label="kmeans" destination="uas/">
        <Feature Name="X" Weight="1"/>
        <Feature Name="Y" Weight="1"/>
        <Feature Name="Z" Weight="1"/>
        <Feature Name="visibility" Weight="1"/>
      </Kmeans>
      <Growable label="growable" destination="uas/">
        <Feature Name="visibility" Weight="1"/>
      </Growable>
      <Hierarchical label="hierarchical" destination="uas/">
        <Feature Name="X" Weight="1"/>
        <Feature Name="Y" Weight="1"/>
        <Feature Name="Z" Weight="1"/>
        <Feature Name="visibility" Weight="1"/>
      </Hierarchical>
      <PGmeans label="pgmeans" destination="uas/">
        <Feature Name="X" Weight="1"/>
        <Feature Name="Y" Weight="1"/>
        <Feature Name="Z" Weight="1"/>
        <Feature Name="visibility" Weight="1"/>
      </PGmeans>
      <Successive label="successive" destination="uas/">
        <PartitioningMethod Method="kmeans"/>
        <PartitioningMethod Method="kmeans"/>
      </Successive>
    </partitioning_methods>

    <partitioning_evaluators>
      <AVGEvaluator label="avg" Feature="visibility"/>
      <STDEvaluator label="std" Feature="visibility"/>
    </partitioning_evaluators-->
    <!-- ** ** -->

    <MPStrategies>

      <!-- BlindRRT -->
      <BlindRRT label="BlindRRT" delta="2.0" minDist="0.1" CCconnection="NodeToNode"
        lpLabel="sl" dmLabel="euclidean" initialSamples="200" ccIters="100"
        nfLabel="BFNF" vcLabel="rapid" connectorLabel="RRTConnect" query="2D/2d.query" debug="true" evaluateGoal="true"/>

      <!-- Basic Parallel PRM -->
      <BasicParallelPRM label="BasicParallelPRM" debug="true">
        <Sampler method="UniformRandomFree" number="10"/>
        <Connector method="Closest"/>
        <Evaluator method="NodesEval"/>
      </BasicParallelPRM>

      <!-- Regular Subdivision -->
      <RegularSubdivisionMethod label="RegularSubdivisionMethod" debug="true">
        <node_generation_method Method="UniformRandomFree" Number="100"/>
        <node_connection_method Method="Closest"/>
        <!--sequential_strategy  Strategy="BasicPRM1"/-->
        <num_row nRow = "4"/>
        <n_col nCol = "4"/>
        <num_runs nRuns = "1"/>
        <overlap xeps = "0.25" yeps = "0.25" zeps = "0"/>
        <region_connect_k k1 = "1" k2 = "1" nf = "BFNF" lp= "sl" type = "random"/>
      </RegularSubdivisionMethod>

      <!-- Bulk Synchronous RRT -->
      <BulkRRT label="BulkRRT" debug="true">
         <k_nodes kNodes = "1"/>
         <vc_method vcm = "rapid"/>
         <evaluation_method Method="NodesEval"/>
         <dm_method Method="euclidean"/>
         <min_distance minDist ="0.1"/>
	 <!--<query query = "2D/2d.query"/>-->
      </BulkRRT>

      <!-- Radial RRT -->
      <RadialSubdivisionRRT label="RadialSubdivisionRRT" debug="false" >
        <!-- the training samples (numTestSamples) should at most equal nmNodes -->
	<region_constr numRegions = "2" rayLength= "50" numNeighbors = "1"/>
	<!-- numAttempts should at least equal numNodes-->
        <rrt_constr numNodes = "100" numAttempts = "20" delta= "4.0" minDist = "0.05" strictBranching="false" />
	<vc_method vcm = "rapid"/>
	<dm_method Method="euclidean"/>
	<connectionMethod Label="RegionConnector"/>
        <nf_method Method="BFNF"/>
        <e_method Method="BERO"/>
      </RadialSubdivisionRRT>

      <!-- Radial Blind RRT -->
      <RadialBlindRRT label="RadialBlindRRT" debug="false" >
        <!-- the training samples (numTestSamples) should at most equal nmNodes -->
	<region_constr numRegions = "2" rayLength= "50" numNeighbors = "1"/>
	<!-- numAttempts should at least equal numNodes-->
        <rrt_constr numNodes = "100" numAttempts = "20" delta= "4.0" numCCIters="25"
          minDist = "0.05" strictBranching="true" CCconnection="NodeToNode"/>
	<vc_method vcm = "rapid"/>
	<dm_method Method="euclidean"/>
	<connectionMethod Label="RegionRRTConnect"/>
        <nf_method Method="BFNF"/>
      </RadialBlindRRT>

    </MPStrategies>

    <Solver mpStrategyLabel="BasicParallelPRM" seed="12345678"
      baseFilename="BasicParallelPRM" vizmoDebug="false"/>
    <!-- MPProblem allows multiple solvers to be specified and executed. -->
    <!--Solver mpStrategyLabel="BasicPRM2" seed="12345678" baseFilename="Basic2" vizmoDebug="true"/-->

  </MPProblem>
</MotionPlanning>
