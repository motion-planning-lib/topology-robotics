/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_SKELETONS_SPANS_HPP
#define STAPL_SKELETONS_SPANS_HPP

#include "spans/balanced.hpp"
#include "spans/tree.hpp"
#include "spans/misc.hpp"

#endif // STAPL_SKELETONS_SPANS_HPP
