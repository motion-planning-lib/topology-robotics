/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_BASE_CONTAINER_BASE_HPP
#define STAPL_CONTAINERS_BASE_CONTAINER_BASE_HPP

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Tag-class for pContainers.
//////////////////////////////////////////////////////////////////////
struct container_base
{ };

} // namespace stapl


#endif // STAPL_CONTAINERS_BASE_CONTAINER_BASE_HPP
